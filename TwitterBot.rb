#!/usr/bin/env ruby -Ku
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

require 'time'
require 'logger'
require 'yaml/store'
require 'rubygems'
require 'oauth'
require 'rubytter'
require 'httpclient'

# bot起動処理を定義したモジュール
module BotLauncher
    def process_command_line()
	case @argv[0]
	when '-cron'
	    if login(:logfile)
		routine_process()
	    end

	when '-once'
	    if login(:stdout)
		routine_process()
	    end

	when '-cgi'
	    print "Status: 200\r\n"
	    print "Content-Type: text/plain; charset=UTF-8\r\n"
	    print "\r\n"
	    if login(:logfile)
		routine_process()
		print "done"
	    else
		print "busy"
	    end

	when '-daemon'
	    if login(:logfile)
		while continue_loop?
		    routine_process()
		    wait_for_next_run()
		end
	    end

	when '-fg'
	    if login(:stdout)
		while continue_loop?
		    routine_process()
		    wait_for_next_run()
		end
	    end

	when '-help'
	    print_help()

	else
	    # TODO: 上記以外の引数が渡された場合は、その名前の関数を呼び出す
	    if not @cgimode
		func = @argv[0].sub(/^-/, '').gsub(/-/, '_')
		self.method(func).call()
	    end
	end
    end

    # ヘルプを表示する
    def print_help()
	[['-cron', '一回メインルーチンを実行して終了する'],
	 ['-once', '-cronと同じだが、標準出力に出力する'],
	 ['-cgi',  '-cronと同じだが、CGI用の出力を行なう'],
	 ['-daemon', 'メインループを実行し続ける'],
	 ['-fg', '-daemonと同じだが、標準出力に出力する']].each do |x|
	    STDERR.print "#{x[0]}\n"
	    STDERR.print "\t#{x[1]}\n"
	end
    end
end



# 抽象botクラス
class TwitterBot
    include BotLauncher
    private

    #----------------------------------------------------------------------
    # 取得・更新数管理

    REMAINING_GET_COUNT = 5 # 動作続行に必要なrate_limit_statusの残り回数

    MAX_UPDATES = 60 # UPDATE_HISTORY_TIMEOUT中にポスト可能な回数

    UPDATE_HISTORY_TIMEOUT = 3600 # update履歴の保持期限

    #----------------------------------------------------------------------
    # 動作インターバル

    ROUTINE_INTERVAL = 5 # 自前で処理ループを作る場合の間隔[s]

    FRIENDSHIP_INTERVAL = 3600 # friendship取得間隔の最小値

    DIRECT_MSG_INTERVAL = 86400 # direct message取得間隔の最小値

    MENTION_INTERVAL = 600 # mention取得間隔の最小値

    TIMELINE_INTERVAL = 30 # timeline取得間隔の最小値(3600[s]/150[回]=24)

    #----------------------------------------------------------------------
    # 設定ファイル・ログファイル

    # デフォルトのログファイル名
    DEFAULT_LOG_FILE = 'twitterbot.log'

    # グローバル設定
    GLOBAL_CONFIG_FILE = 'config_v2.yaml'
    GLOBAL_CONFIG_VERSION = 2

    # グローバル状態
    GLOBAL_STATE_FILE = 'global_state_v2.yaml'

    # 空白文字
    NBSP = [0xa0].pack('U')	# non-breaking space
    FWSP = '　'			# fullwidth space
    WS = "[#{FWSP}#{NBSP}\\s]"	# any white space

    #----------------------------------------------------------------------
    public

    # オブジェクトの初期化
    def initialize(argv, basedir = ".")
	@argv = argv.clone
	@basedir = basedir.clone
	if @argv.size > 0 && @argv[0] == '-cgimode'
	    @cgimode = true
	    @argv.shift
	end

	@client = nil
	@logger = nil
	@gconf = nil

	@curr_obj = nil		# メインループ実行時の時刻
	@curr_time = 0		# メインループ実行時の時刻

	@rate_limit_info = nil	# rate_limit_statusオブジェクトのキャッシュ
	@post_banned = false	# ポスト処理が規制を受けているか

	@quit_flag = false	# メインループの終了要求フラグ
    end

    #----------------------------------------------------------------------
    # メイン処理
    public

    # メイン処理を実行する。起こってよい例外はここ以下でキャッチする
    def routine_process()
	# メインループごとにグローバル設定を読み込んでいる。
	# 若干無駄だが、ループ間で設定ファイルを手動で編集できる
	@gconf = load_global_config()

	@gconf.transaction do
	    routine_process_locked()
	    @gconf.commit()
	end
    end

    # 実行を継続するか判定する
    def continue_loop?()
	return !@quit_flag
    end

    # 次のメイン処理を行なうための待ち処理
    def wait_for_next_run()
	@logger.debug("waiting for #{ROUTINE_INTERVAL} seconds")
	sleep(ROUTINE_INTERVAL)
    end

    private

    # メインループ実体。
    # この処理の内部ではグローバル設定はロックされている。
    def routine_process_locked()
	@logger.info("*" * 40)
	@curr_obj = Time.now
	@curr_time = @curr_obj.to_i

	# updatesカウントのうち、古いエントリを削除する
	purge_update_history()

	# 取得系の制限をチェックし、ひっかかりそうなら抜ける
	@logger.debug("checking limit rate")
	@rate_limit_info = invoke_api("limit_status") { @client.limit_status() }
	if too_much_fetch?(@rate_limit_info)
	    @logger.warn("detected too much fetch, exitting")
	    return
	end
	if @gconf[:fetch_min_remain] > @rate_limit_info.remaining_hits
	    @gconf[:fetch_min_remain] = @rate_limit_info.remaining_hits
	end

	# friendship情報のキャッシュを更新する
	if @curr_time - @gconf[:friendship_seen_at] > FRIENDSHIP_INTERVAL
	    @logger.debug("updating friendship cache")
	    refresh_friendship_cache()
	    @gconf[:friendship_seen_at] = @curr_time
	end

	# メッセージ・ステータス類を処理
	# routine_process -> process_xxx -> respond_for_xxx
	process_direct_message()
	process_monologue()
	process_mention()
	process_timeline()
    end


    #----------------------------------------------------------------------
    # direct message受信

    # direct message受信処理。
    # 取得したdirect messageに対してrespond_for_dmsg()を呼び出す
    def process_direct_message()
	if @curr_time <= @gconf[:direct_msg_seen_at] + DIRECT_MSG_INTERVAL
	    return
	end

	last_id = @gconf[:direct_msg_last_id]
	opt = { :count => 200, :since_id => last_id }
	@logger.debug("finding direct messages newer than #{last_id}")
	dmsgs = invoke_api("direct_messages", last_id) do
	    @client.direct_messages(opt)
	end
	return if dmsgs == nil

	dmsgs.reverse_each do |msg|
	    next if msg.id <= last_id

	    @logger.info("new_dmsg: #{msg.id} " \
			 + "from #{msg.sender.screen_name}")
	    @logger.info("\t#{msg.text}")

	    uconf = load_user_config(msg.sender)
	    begin
		uconf.transaction do
		    respond_for_dmsg(msg, uconf)
		    uconf.commit()
		end
		if @gconf[:direct_msg_last_id] < msg.id
		    @gconf[:direct_msg_last_id] = msg.id
		end
	    rescue Timeout::Error, StandardError => ex
		log_exception(ex, "respond_for_dmsg failed")
		break
	    end
	end
	@gconf[:direct_msg_seen_at] = @curr_time
    end

    # direct messageごとの受信処理。
    # TwitterBot#respond_for_dmsg()では何もしないデフォルト実装を定義する
    def respond_for_dmsg(msg, uconf)
    end

    #----------------------------------------------------------------------
    # 自発発言

    # 自発発言処理。
    # respond_for_monologue()を呼び出す
    def process_monologue()
	# つぶやきを生成してポスト
	begin
	    respond_for_monologue()
	rescue Timeout::Error, StandardError => ex
	    log_exception(ex, "respond_for_monologue failed")
	    return
	end
    end

    # 実際の自発発言処理
    # TwitterBot#respond_for_monologue()では何もしないデフォルト実装を定義する
    def respond_for_monologue()
    end

    #----------------------------------------------------------------------
    # mention処理

    # mention受信処理。
    # respond_for_mention()を呼び出す。
    def process_mention()
	if @curr_time <= @gconf[:mention_seen_at] + MENTION_INTERVAL
	    return
	end

	last_id = @gconf[:mention_last_id]
	opt = { :count => 200, :since_id => last_id }
	@logger.debug("finding mentions newer than #{last_id}")
	mentions = invoke_api("mentions", last_id) do
	    @client.mentions(opt)
	end
	return if mentions == nil

	mentions.reverse_each do |status|
	    next if status.id <= last_id
	    ctime, hms, diff = *time_info(status.created_at, @curr_obj)
	    @logger.debug("new_mention: id #{status.id}, at #{hms}, latency #{diff}, from #{status.user.screen_name}, #{status.text}")
	    uconf = load_user_config(status.user)
	    begin
		uconf.transaction do
		    respond_for_mention(status, uconf)
		    uconf.commit
		end
		if @gconf[:mention_last_id] < status.id
		    @gconf[:mention_last_id] = status.id
		end
	    rescue Timeout::Error, StandardError => ex
		log_exception(ex, "respond_for_mention failed")
		break
	    end
	end
	@gconf[:mention_seen_at] = @curr_time
    end

    # mentionごとの受信処理
    # TwitterBot#respond_for_mention()では何もしないデフォルト実装を定義する
    def respond_for_mention(status, uconf)
    end

    #----------------------------------------------------------------------
    # timeline処理

    # timeline受信処理。
    # respond_for_timeline()を呼び出す。
    def process_timeline()
	if @curr_time <= @gconf[:timeline_seen_at] + TIMELINE_INTERVAL
	    return
	end

	last_id = @gconf[:timeline_last_id]
	opt = { :count => 200, :since_id => last_id }
	@logger.debug("finding statuses newer than #{last_id}")
	timeline = invoke_api("friends_timeline", last_id) do
	    @client.friends_timeline(opt)
	end
	return if timeline == nil

	timeline.reverse_each do |status|
	    next if status.id <= last_id
	    ctime, hms, diff = *time_info(status.created_at, @curr_obj)
	    @logger.info("new status: id #{status.id}, at #{hms}, latency #{diff}, from #{status.user.screen_name}, #{status.text}")
	    uconf = load_user_config(status.user)
	    begin
		uconf.transaction do
		    respond_for_timeline(status, uconf)
		    uconf.commit()
		end
		if @gconf[:timeline_last_id] < status.id
		    @gconf[:timeline_last_id] = status.id
		end
	    rescue Timeout::Error, StandardError => ex
		log_exception(ex, "respond_for_timeline failed")
		break
	    end
	end
	@gconf[:timeline_seen_at] = @curr_time
    end

    # timeline上のstatusごとの受信処理。
    # TwitterBot#respond_for_timeline()では何もしないデフォルト実装を定義する
    def respond_for_timeline(status, uconf)
    end

    #----------------------------------------------------------------------
    # ログイン処理/設定読み込み

    # グローバル設定・グローバル状態を読み込む
    def load_global_config()
	conf = YAML::Store.new(File::join(@basedir, GLOBAL_STATE_FILE))
	conf.transaction do
	    set_if_unset(conf, :config_version, GLOBAL_CONFIG_VERSION)
	    set_if_unset(conf, :friends, [])
	    set_if_unset(conf, :update_history, Hash.new)
	    set_if_unset(conf, :friendship_seen_at, 0)
	    set_if_unset(conf, :direct_msg_seen_at, 0)
	    set_if_unset(conf, :direct_msg_last_id, 1)
	    set_if_unset(conf, :mention_seen_at, 0)
	    set_if_unset(conf, :mention_last_id, 1)
	    set_if_unset(conf, :timeline_seen_at, 0)
	    set_if_unset(conf, :timeline_last_id, 1)
	    set_if_unset(conf, :fetch_min_remain, 9999)
	    set_if_unset(conf, :post_min_remain, 9999)
	    conf.commit()

	    # グローバル設定のバージョンをチェック
	    if conf[:config_version] != GLOBAL_CONFIG_VERSION
		raise "unexpected version for global configuration"
	    end
	end
	return conf
    end

    # ユーザごとの設定ファイルを読み込む。
    # デフォルト実装では何も保持しないオブジェクトを返す。
    def load_user_config(user)
	conf = Object.new
	def conf.transaction(&blk)
	    yield blk
	end
	def conf.commit
	end
	return conf
    end

    # キーが設定されてない場合はデフォルト値を設定する
    def set_if_unset(conf, key, default_value)
	if not conf.root?(key)
	    conf[key] = default_value
	end
    end

    # 設定されている値を上下限値で制限する
    def limit_value(conf, member, min, max)
	conf[member] = min if conf[member] < min
	conf[member] = max if conf[member] > max
    end

    public

    # Twitterにログインし、動作環境を整える
    # - OAuth認証する場合はグローバル設定で:use_oauthをtrueに設定し、
    #	さらに関連キー・トークンを設定しておく
    # - BASIC認証の場合は:passwordだけでよい
    # - ただし、いずれの認証方式でも:user_nameと:passwordは設定しておく必要あり
    def login(logfile = nil)
	# ログ出力環境を設定(レベルは後で再設定する)
	if logfile == nil || logfile == :logfile
	    logfile = File::join(@basedir, DEFAULT_LOG_FILE)
	else
	    logfile = STDOUT
	end
	@logger = Logger.new(logfile, 'daily')
	@logger.datetime_format = "%m/%d %H:%M:%S"

	# 排他的実行を行うためにファイルをロック
	lockfile = File.open("#{@basedir}/lockfile", "w")
	if lockfile.flock(File::LOCK_EX | File::LOCK_NB) == false
	    @logger.warn("exclusion failed")
	    return false
	end

	# クライアントの作成とアイデンティティの設定
	acct = YAML::Store.new(File::join(@basedir, GLOBAL_CONFIG_FILE))
	acct.transaction(true) do
	    @user_name = acct[:user_name]
	    @password = acct[:password]
	    @admin = acct[:admin]

	    if acct.root?(:use_oauth) && acct[:use_oauth]
		oauth = Rubytter::OAuth.new(acct[:consumer_key],
					       acct[:consumer_secret])
		token = OAuth::AccessToken.new(oauth.create_consumer(),
					       acct[:access_token],
					       acct[:access_token_secret])
		@client = OAuthRubytter.new(token)
	    else
		@client = Rubytter.new(acct[:user_name], acct[:password], api_opt)
	    end

	    if acct.root?(:debug) && acct[:debug]
		@logger.level = Logger::DEBUG
	    else
		@logger.level = Logger::INFO
	    end
	end
	return true
    end

    # 情報取得インターバルをリセットし、
    # 次回実行時に必ず情報を取得するようにする
    def refresh_intervals()
	@gconf = load_global_config()
	@gconf.transaction do
	    @gconf[:friendship_seen_at] = 0
	    @gconf[:direct_msg_seen_at] = 0
	    @gconf[:mention_seen_at] = 0
	    @gconf[:timeline_seen_at] = 0
	    @gconf.commit()
	end
    end

    #----------------------------------------------------------------------
    # 規制状態把握用のキャッシュ/履歴処理
    private

    # GET系API規制にひっかかりそうかチェックする
    # GET系APIの使用可能回数がREMAINING_GET_COUNT以下であればtrueを返す
    def too_much_fetch?(limit)
	return true if limit == nil
	@logger.info("get_limit: " \
		     + "#{limit.hourly_limit} limits, " \
		     + "#{limit.remaining_hits} remains")
	return limit.remaining_hits < REMAINING_GET_COUNT
    end

    # 古い更新履歴を破棄する
    def purge_update_history()
	@gconf[:update_history].each_pair do |time, count|
	    if time + UPDATE_HISTORY_TIMEOUT < @curr_time
		@logger.info("purging obsoleted update history: #{time}")
		@gconf[:update_history].delete(time)
	    end
	end
    end

    # POST系API規制にひっかかりそうかチェックする
    # 直近のポスト回数が最大ポスト回数*coef以上ならばtrueを返す
    def too_many_posts?(coef = 1)
	nr = @gconf[:update_history].size
	thre = ([60,50,40,30,20,20, # 00-05
		 30,50,40,40,40,40, # 06-11
		 40,30,30,30,30,40, # 12-17
		 50,50,60,60,60,60, # 18-23
		][@curr_obj.hour] * coef).to_i
	if nr > thre
	    @logger.info("post_limit: nr_updates=#{nr}, max=#{MAX_UPDATES}, thre=#{thre}")
	    return true
	else
	    return false
	end
    end

    #----------------------------------------------------------------------
    # friend関係の処理

    def refresh_friendship_cache()
	ids = invoke_api("friends_ids") { @client.friends_ids(@user_name) }
	return if ids == nil
	@gconf[:friends] = ids

	ids = invoke_api("followers_ids") { @client.followers_ids(@user_name) }
	return if ids == nil
	@gconf[:followers] = ids
    end

    def is_friend(f_user)
	return @gconf[:friends].include?(f_user.id)
    end

    # friendを追加する。
    # 追加に失敗した場合(すでにfriendであったりタイムアウトの場合)はnilを返す
    # 成功した場合はUserの構造体を返す
    def add_friend(f_name)
	f_user = invoke_api("follow") { @client.follow(f_name) }
	return nil if f_user == nil

	@gconf[:friends] << f_user.id
	return f_user
    end

    # friendを削除する。
    # 削除に失敗した場合(すでにfriendでなかったりタイムアウトの場合)はnilを返す
    # 成功した場合はUserの構造体を返す
    def remove_friend(f_name)
	f_user = invoke_api("leave") { @client.leave(f_name) }
	return nil if f_user == nil

	@gconf[:friends].delete(f_user.id)
	return f_user
    end

    #----------------------------------------------------------------------
    # ポスト処理

    # direct messageを送信する。
    # 送信に失敗した場合はnilを返す(例外は発生しない)。
    def post_direct_message(user, string)
	@logger.info("post_direct_message to #{user}: #{string}")
	args = {:user => user, :text => string}
	return invoke_api("send_direct_message",
			  "user=#{user}, text=#{string})") do
	    @client.send_direct_message(args)
	end
    end

    # statusを送信する
    # 送信に失敗した場合はnilを返す(例外は発生しない)。
    # 送信失敗状態が変化した場合(おそらくポスト規制時と解除時)は
    # post_ban_state_changed()を呼び出す
    def post_status(string, opt = nil)
	@logger.info("post_status: #{string}")
	args = {:status =>string}
	args[:in_reply_to_status_id] = opt.id if opt != nil
	status = invoke_api("update_status", "text=#{string}") do
	    @client.update_status(args)
	end

	if status == nil
	    post_ban_state_changed(true) if @post_banned == false
	    @post_banned = true
	else
	    post_ban_state_changed(false) if @post_banned == true
	    @post_banned = false
	    @gconf[:update_history][@curr_time] = status.id
	    post_remain = MAX_UPDATES - @gconf[:update_history].size
	    if @gconf[:post_min_remain] > post_remain
		@gconf[:post_min_remain] = post_remain
	    end
	end
	return status
    end

    # 送信失敗状態が変化した時の処理。
    # デフォルト実装では何もしない
    def post_ban_state_changed(new_ban_state)
    end

    #----------------------------------------------------------------------
    # その他ユーティリティー(外部サービス)

    SHINDAN_MAKER_URL = 'http://shindanmaker.com'

    # "診断メーカー"(http://shindanmaker.com)での診断を行なう
    def get_shindan_maker(id, user)
	client = HTTPClient.new
	url = "#{SHINDAN_MAKER_URL}/#{id}"
	resp = client.post_content(url, {'u' => user})
	if resp =~ %r(a href="http://twitter.com/home\?status=([^\"]*)")
	    return URI.decode($1).gsub('+', ' ')
	else
	    return nil
	end
    end

    # かなこん(http://t-rack.net/kanakon/)
    def register_kanakon(kanalist)
	kanalist.reverse_each do |kana|
	    add_friend("Kanakon_#{kana}")
	    sleep(2)
	end
    end

    def unregister_kanakon(kanalist)
	kanalist.each do |kana|
	    remove_friend("Kanakon_#{kana}")
	end
    end

    #----------------------------------------------------------------------
    # その他ユーティリティー(ユーティリティー)

    # 時刻を表わす文字列からTimeオブジェクトを返す
    def parse_date(s)
	begin
	    return Time.parse(s)
	rescue
	    return Time.at(0)
	end
    end

    # ポストに関する時刻情報の配列を返す
    # 引数
    #   string = 時刻を表わす文字列
    #   machine_time = マシン時刻
    # 返り値 = [time, hms, diff]
    #   time = 時刻
    #   hms = timeのうち、時刻情報のみを文字列表記にしたもの
    #   diff = timeとマシン時刻の差分(単位は秒)
    def time_info(string, machine_time)
	time = parse_date(string)
	hms = time.strftime("%H:%M:%S")
	diff = (time - machine_time).to_i
	return [time, hms, diff]
    end

    API_MAX_RETRIES = 2

    # リトライ付きでAPIを呼び出す。
    # API呼び出しが失敗した場合、指定された名前と記述をログに出力する。
    # API呼び出しが失敗した場合は例外は発生せず、返り値がnilになる。
    # 例:
    # result = invoke_api("some_api") do
    #	  @client.some_api(arg1, arg2)
    # end
    def invoke_api(name, desc = nil)
	retries = 0
	begin
	    yield
	rescue Timeout::Error, StandardError => ex
	    retries += 1
	    if retries < API_MAX_RETRIES
		@logger.debug("#{name} failed, retrying...")
		retry
	    end

	    if desc == nil
		log_exception(ex, "#{name} failed, gave up")
	    else
		log_exception(ex, "#{name}(#{desc}) failed, gave up")
	    end
	    return nil
	end
    end

    # 例外のバックトレースをログに出力する
    def log_exception(ex, desc = nil)
	a = []
	a << desc if desc
	a << ex.to_s
	ex.backtrace.each do |bt|
	    a << "\tfrom #{bt}"
	end
	@logger.error(a.join("\n"))
    end

    # 与えられたArrayからランダムに要素を取り出す
    def rand_item(array)
	return array[rand(array.size)]
    end

    # ツイートの形式を判定し、宛名リスト・本文・RT宛先・RT部に分解する。
    # 判定できる形式は以下のとおり
    #
    # - リプライ形式(:reply)		@user1 @user2 本文
    # - メンション形式(:mention)	. @user1 @user2 本文
    # - 非公式RT形式(:rt)		本文 RT @rt_to: rt_body
    # - その他(:plain)			本文
    #
    # 返り値として、以下のメンバーを保持するHashを返す
    # - type: ツイートの形式。:reply, :mention, :rt, :plainのいずれか
    # - original: もともとのツイート
    # - body: ツイート本文。リプライ先やRT部を含まない
    # - r_to: (リプライ形式のみ) リプライ先を保持する配列
    # - m_to: (メンション形式のみ) 言及先を保持する配列
    # - rt_to: (非公式RT形式のみ) RT元のツイートの発信ユーザ
    # - rt_body: (非公式RT形式のみ) RT元のツイートの本文
    #
    # このハッシュはさらにあて先のクラスを示す以下のメンバーを
    # オプションで保持する。
    # - except_me:  自分を明示的に除外した返信だったときtrue
    # - to_me:      明示的に自分を指定した返信だったときtrue
    # - only_to_me: 明示的に自分だけを指定した返信だったときtrue
    # 
    def decompose_twit(text, name)
	s = text.sub(/^#{WS}*/mx, '')
	r = {:type => nil, :original => text,
	    :r_to => [], :m_to => [], :rt_to => [],
	    :body => '', :rt_body => ''}

	if s[0] == ?@
	    r[:type] = :reply # リプライ形式
	    while s.sub!(/^@([a-zA-Z0-9_]+)#{WS}*/mx, '')
		r[:r_to] << $1
	    end
	    r[:body] = s
	    if r[:r_to].include?(name)
		# 明示的に自分を指定して返信
		r[:to_me] = true
		r[:only_to_me] = true if r[:r_to].length == 1
	    else
		# 明示的に自分を含めない(他の人のみ)
		r[:except_me] = true
	    end

	elsif s[0] == ?.
	    r[:type] = :mention # メンション形式
	    s.sub!(/^\.#{WS}*/mx, '')
	    while s.sub!(/^@([a-zA-Z0-9_]+)#{WS}*/mx, '')
		r[:m_to] << $1
	    end
	    r[:body] = s
	    if r[:m_to].include?(name)
		# 明示的に自分を指定して返信
		r[:to_me] = true
		r[:only_to_me] = true if r[:m_to].length == 1
	    else
		# 明示的には自分を指定していない
	    end

	elsif s =~ /^(.*?)#{WS}*[RQ]T:?#{WS}*@([a-zA-Z0-9_]+):?#{WS}*(.*)$/mx
	    # 非公式RT形式 (明示的には自分を指定していない)
	    r[:body] = $1
	    r[:rt_to] << $2
	    r[:rt_body] = $3
	    r[:type] = :rt

	else
	    # いずれでもない (明示的には自分を指定していない)
	    r[:body] = s
	    r[:type] = :plain
	end
	return r
    end
end
