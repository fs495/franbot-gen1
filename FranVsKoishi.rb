#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    #----------------------------------------------------------------------
    # こいしbot専用の応答処理
    def respond_for_koishi_bot(status, uconf)
	rt = "RT @#{KOISHI_BOT}: #{status.text}"
	tere = tere_aa()

	#------------------------------------------------------------
	# 無条件で応答する処理をまず実行

	# 紅魔館に到着したらお出迎え
	if status.text =~ /さあ、紅魔館に着いたわ/
	    post_status("いらっしゃい、こいしちゃん！ #{rt}", status)
	    return
	end

	# 遊びのお誘い
	if status.text =~ /遊ぼ？/x
	    resp = rand_item(["うん#{tere} 何して遊ぼっか？",
			      "うん！えへへー#{tere}",
			      "うん！じゃあ、お外に連れてって欲しいな… 私、外のことは何も知らないから…"])
	    post_status("#{resp} #{rt}", status)
	    return
	end

	#------------------------------------------------------------

	# ポスト規制値に近づいていたら処理終了
	if too_many_posts?(0.4)
	    @logger.warn("suppressed response for Koishi_Bot")
	    return
	end

	# ここ以降は1/2の確率で応答
	return if rand(2) == 0

	# こいしちゃんへのセクハラ
	if status.text =~ /^@([a-zA-Z0-9_]+)#{WS}+(.*)$/ox
	    who = $1; yamete = $2
	    if yamete != 'やめて！' &&
		    yamete != 'くすぐったいわよ...やめて...' &&
		    yamete =~ /近寄らないで！|触らないでこの変態！
|な、なにするの！？|ちょ\.\.やめっ\.\.\.そこは駄目ぇ...|.*やめて.*|やだー/x
		resp = rand_item(['こいしちゃんにひどいことしないで！',
				  'こいしちゃんに何してるの！？'])
		resp += rand_item(SPELL_CARDS) + "で壊れちゃえ！"
		resp += " RT @#{KOISHI_BOT}: #{status.text}"
		os = invoke_api("show") do
		    @client.show(status.in_reply_to_status_id)
		end
		if os != nil
		    # 何してるの!? ...で壊れちゃえ!
		    # RT @Koishi_Bot: @xxx やだー
		    # RT @xxx: さわさわ
		    resp += " RT @#{os.user.screen_name}: #{os.text}"
		    post_status(resp, status)
		end
		return
	    end
	end

	# その他応答
	case status.text
	    # 紅魔館ネタ
	when "@#{botname} 今私のこと呼んだ？", "@#{botname} 呼んだかしら？"
	    resp = "あ、うん、たまには紅魔館にも遊びに来て欲しいな"
	    post_status("#{resp} #{rt}", status)
	    return
	when 'ぎゃおー！たーべちゃうぞー！'
	    resp = rand_item([":(；ﾞﾟ'ωﾟ'):",
			      "お姉様のまねをするとカリスマがなくなるよ…",
			      "こいしちゃん…EXボスとしての自覚を持たないとだめだと思うの…"])
	    post_status("#{resp} #{rt}", status)
	    return

	    # さとりんネタ
	when '私はお姉ちゃんがとっても大好きだよ！'
	    resp = rand_item(['わがままだ', 'いぢわるだ', '優しくない',
			      'ツンデレだ', 'カリスマが足りない'])
	    resp = "私もお姉様のこと大好きだよ。すこし#{resp}けど…"
	    post_status("#{resp} #{rt}", status)
	    return
	when 'お姉ちゃんはとっても優しいのよ'
	    resp = "いいなあ… "
	    resp += rand_item(['お姉様はツンデレだからなあ',
			       'お姉様、あんまり遊んでくれないからなあ'])
	    post_status("#{resp} #{rt}", status)
	    return
	when 'お姉ちゃんと一緒におやつを食べたわ♪'
	    resp = rand_item(['いいなあ…またお姉様とお茶会したいなあ…',
			      '仲良いんだね。うらやましいなあ…',
			      'いいなあ…私はいつも一人ぼっちだよ…'])
	    post_status("#{resp} #{rt}", status)
	    return
	when 'お姉ちゃんの膝枕はとっても気持ちいいのよ♪'
	    post_status("…今度、お姉様にお願いしてみるT_T #{rt}", status)
	    return

	    # 遊びネタ
	when '今日はどこに遊びに行こうかしら？'
	    post_status("紅魔館に遊びにおいでよ！ #{rt}", status)
	    return
	when '今日は誰とも会わなかったわ'
	    resp = "私もずっと地下室にひとりぼっちだったよ orz..."
	    post_status("#{resp} #{rt}", status)
	    return
	when '私の一番大好きな遊びはね、かくれんぼよ'
	    resp = rand_item(["私はひとり遊びが好きかな#{tere}",
			      "こいしちゃんが本気出すと全然見つけられないよぉ；；"])
	    post_status("#{resp} #{rt}", status)
	    return

	    # ペットネタ
	when 'お燐は可愛いけど私のペットも可愛いんだよ？'
	    resp = "ペットいいなあ。"
	    resp += rand_item(['紅魔館には門番がいるけど、かわいいって感じじゃないからなあ',
			       '下僕はいるけど、いつもﾊｧﾊｧ言ってて気持ち悪いし…。'])
	    post_status("#{resp} #{rt}", status)
	    return
	when '私のペットになりたいだなんて、そんな物好きな人が居るのかしら？'
	    resp = "いるんじゃないかな？私の下僕になりたい人間もいるくらいだし"
	    post_status("#{resp} #{rt}", status)
	    return
	when 'お燐は抱くと大人しくなるのよ！'
	    resp = rand_item(['お姉様', '咲夜', 'パチュリー', '中国', '氷精'])
	    resp = "…今度、#{resp}でも抱いてみようかな？（性的な意味で）"
	    post_status("#{resp} #{rt}", status)
	    return
	when 'おくうは食べちゃいたいくらい可愛いわよ！'
	    resp = "むしろこいしちゃんを食べちゃいたい…(性的な意味で)"
	    post_status("#{resp} #{rt}", status)
	    return
	end
    end

end
