#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    #----------------------------------------------------------------------
    # レミリアbot専用の処理
    def respond_for_remilia_bot(status, uconf)
	# レミリアbot側で応答できるものについてはリプライで返し、
	# そうでないものはRTで返して無視させる
	to = "@#{REMILIA_BOT} "
	rt = "RT @#{REMILIA_BOT}: #{status.text}"
	tere = tere_aa()

	# 無条件に反応する処理
	case status.text
	when /@#{botname} ……一緒に寝る？/ox
	    post_status("うん！#{tere} #{rt}", status)
	    rand_item([
		       [# 過去の思い出バージョン
			'一緒に寝ていると、昔のこと思い出すね…',
			'ねえ、前みたいに「お姉ちゃん」って呼んでいい？',
			'ちぇー、ケチ… せっかくのデレルートのフラグだったのに！',
			'…あれえ、もしかして照れてる？',
			'やだ、私まで恥ずかしくなってきちゃった///',
			'そっ、それじゃおやすみなさい！ …お姉ちゃん♥'
		       ],
		       [# 最終鬼畜妹バージョン
			'じゃあいつもどおり、私からいくね',
			'うふふ♥',
			'…あれえ、いつものツンツンな態度はどこにいったのかなあ♪',
			'夜の女王とか言ってるわりには、とんだヘタレ受けね',
			'いい声で啼くじゃない？お姉様♥',
			'あれあれ、だらしないなあ。カリスマも形なしね。ｸｽｯ',
		       ],
		       [# ヤンデレバージョン
			'お姉様、もっと構って欲しいの…',
			'咲夜ばっかりかわいがってずるいよ…',
			'今日は私だけのものになって！',
			'ﾊｧﾊｧ…',
			'うふふ、お姉様かわいい♥',
			'…ふぅ…おやすみなさい',
		       ],
		       [# デレバージョン
			'ベッド一緒なんて、久しぶりだね…',
			'ひゃうん、変なとこ触らないでよ(///ω///)',
			'お姉様…おやすみなさい♥',
		       ],
		      ]).each do |resp|
		post_status(resp, status)
	    end
	    return
	end

	# ポスト規制値に近づいていたら処理終了
	if too_many_posts?(0.2)
	    @logger.warn("suppressed response for remilia_bot")
	    return
	end

	# 優先度の高い応答
	case status.text
	when /^@.*たまにはフランと遊んであげようかしら$/x
	    post_status("お姉様#{tere} #{rt}", status)

	when /^@.*最近あの子と遊んであげてないわね$/x
	    post_status("#{to}たまには一緒に遊んで欲しいな…", status)

	when /^@.*弾幕ごっこならフランとやってあげてくれないかしら$/x
	    post_status("#{to}私はお姉様と弾幕ごっこしたいよ…", status)
	end

	# 1/4の確率でその他の応答
	return if rand(4) != 0

	# 優先度の低い応答
	case status.text
	    #--------------------------------------------------
	when 'ぎゃおー！たーべちゃうぞー！'
	    resp = rand_item(["#{to}お姉様…かっこいい#{tere}",
			      "お姉様…カリスマが聞いてあきれるわ #{rt}"])
	    post_status(resp, status)

	when /#{bh}ふふ、ありがと。/ox
	    post_status("＼カリスマ／ #{rt}", status)

	    #--------------------------------------------------
	when '暇ね…'
	    resp = rand_item(["#{to}たまには私と遊んでよ！",
			      "私も… #{rt}",
			      "#{to}パチェから本借りてこようか？",
			      "#{to}図書館から本持ってこようか？",
			      "#{to}小悪魔に本持ってきてもらおうか？",
			      "#{to}咲夜にお茶にしてもらおうよ"])
	    post_status(resp, status)

	when /#{bh}そうね、じゃあ弾幕ごっこでもしましょうか/ox
	    post_status("やった！ありがとうお姉様#{tere} #{rt}", status)

	when /#{bh}(ありがとうフラン、お願いしようかしら|そうね、お願いフラン)/ox
	    post_status("うん、まかせてお姉様！ #{rt}", status)

	when /#{bh}そうね。咲夜ー、お茶にしましょう/ox
	    post_status("お姉様とお茶会嬉しいな♪ #{rt}", status)

	    #--------------------------------------------------
	when 'フランはどこにいったのかしら'
	    resp = rand_item(['わたしはここにいるよ？',
			      'お姉様が地下室に閉じ込めたの忘れたの？'])
	    post_status("#{to}#{resp}", status)

	when /#{bh}あら、いたのフラン。/ox
	    post_status("お姉様ひどい;; #{rt}", status)

	when /#{bh}ご、ごめんなさいフラン。ちゃんと覚えているわよ/ox
	    post_status("ずっといい子にしてたんだから… #{rt}", status)

	    #--------------------------------------------------
	when '霊夢、遊び来ないかしら。'
	    resp = rand_item(["#{to}霊夢じゃなくて私と遊んでよ…",
			      "お姉様のバカ… #{rt}"])
	    post_status(resp, status)

	end
    end
end
