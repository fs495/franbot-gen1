#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    # 遊び (若干好感度上がりやすい)
    def asobi(c)
	return true if c.to_me && !c.only_to_me
	return false unless c.only_to_me

	case c.text
	when /弾幕.*(ごっこ|遊ぶ|遊び|あそび)/x
	    spell = rand_item(SPELL_CARDS)
	    resp = rand_item(['じゃあいくよ！',
			      '壊れちゃダメだよ？',
			      '楽しませてくれるよね？'])
	    post_status("@#{c.nm} #{resp}#{spell}", c.status)
	    c.uconf[:affection] += dice(1, 20)
	    return true

	when /気合い避け/x
	    resp = rand_item(['えいっ、当たっちゃえ！',
			      'えーん、なんで当たらないのー'])
	    post_status("@#{c.nm} #{resp}", c.status)
	    return true

	when /夜(遊び|あそび)|深夜徘徊/x
	    post_status("@#{c.nm} 夜遊びしたいけど、お姉様が外に出してくれないからなあ", c.status)
	    return true

	when /(鬼|おに|オニ)ごっこ/x
	    post_status("@#{c.nm} 私、一応鬼なんだけど…", c.status)
	    return true

	when /かくれんぼ/x
	    post_status("@#{c.nm} わかった！じゃあ、隠れるよ？", c.status)
	    post_status("@#{c.nm} 秘弾「そして誰もいなくなるか？」…これはちょっとズルかったかな♪えへへっ", c.status)
	    return true

	when /(医者|いしゃ).*ごっこ/x
	    resp = "お医者さんごっこ？"
	    resp += rand_item(["#{c.cn}を解剖して遊ぶんだっけ？",
			       "「はじいしゃ」のことだっけ？"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    return true

	when /おままごと/x
	    post_status("@#{c.nm} あの、その、私、お姉様しか家族がいないから、どうやって遊ぶのか分からないの… ご、ごめんなさい…", c.status)
	    return true

	when /(あそ|遊)(んで|ぼう|ぼー|ぼ〜|ばない|びしない)/x
	    if c.tsundere < 0
		post_status("@#{c.nm} じゃあ、#{c.cn}を壊して遊ぼっかな♪", c.status)
	    else
		post_status("@#{c.nm} わーい、何して遊ぶ？", c.status)
	    end
	    c.uconf[:affection] += dice(1, 12)
	    return true
	end
	return false
    end

    # せくはらとすきんしっぷ
    SH_REACTION_TABLE = {
	:kanjiru => ["ひゃぅ！ 変な声出ちゃった…",
		     "ふにゃあ…",
		     "ふにゃん",
		     "ふにゅう…",
		     "ふ、ふぁあ…",
		     "やぁっ…",
		     "やぁん…", 
		     "や、やぁっ…",
		     "やんっ…",
		     "ら、らめぇ…",],
	:shigeki => ["あっ…",
		     "ひゃう",
		     "ひゃうっ",
		     "ひゃんっ",
		     "んんっ…"],
	:iyagaru => ["そ、そんなとこだめぇ…",
		     "だ、だめだよぉ…",
		     "や、やめてよう…",
		     "ふぁ…や、やめてよう…",
		     "い、いやぁ…"],
	:kusuguri => ["くすぐったいよぉ…"] * 2,
	:teretere => ["えへへー",
		      "えへへっ",
		      "う、うにゅう…"],
	:nadenade => ["えへへ、気持ちいい…",
		      "もっとしてぇ…",
		      "も、もっとぉ…"],
	:batou => ["壊れちゃえこの変態！",
		   "こ、壊されたいみたいだね！",
		   "変態！変態！変態！",
		   "変態！爆発しちゃえ！"]
    }
    GENERIC_KANJIRU = SH_REACTION_TABLE[:kanjiru]
    GENERIC_BATOU = SH_REACTION_TABLE[:batou]

    SH_WORD_TO_REACTIONS = [
			    [ /さわさわ/x, [:kanjiru, :iyagaru, :kusuguri]],
			    [ /すりすり/x, [:kanjiru, :iyagaru, :kusuguri]],
			    [ /もふもふ/x, [:kanjiru, :kusuguri]],
			    [ /ふにふに/x, [:kanjiru, :iyagaru]],
			    [ /ぷにぷに/x, [:kanjiru, :iyagaru]],
			    [ /つんつん/x, [:shigeki, :iyagaru]],
			    [ /(ぎゅー?っ?|ギュー?ッ?|ｷﾞｭｰ?ｯ?)#{EOL}$/,
			      [:shigeki, :teretere]],
			    [ /(なでなで|ナデナデ)/x,
			      [:kusuguri, :teretere, :nadenade]],
			    [ /(いちゃいちゃ|イチャイチャ|ｲﾁｬｲﾁｬ)/x,
			      [:kusuguri, :teretere]],
			    [ /(こちょこちょ|コチョコチョ|ｺﾁｮｺﾁｮ)/x,
			      [:iyagaru, :kusuguri]],
			   ]

    # 行為/擬音と行為対象で条件分けする。
    # より高度なものを先に判定する。
    def sekuhara(c)
	return true if c.to_me && !c.only_to_me
	return false unless c.only_to_me
	to = "@#{c.nm}"
	rt = "RT @#{c.nm}: #{c.rawtext}"
	tere = tere_aa()

	case c.text
	when /(えっ?ち|エッチ|H|セック(ス|ル)|ｾｯｸ(ｽ|ﾙ)|[Ss][Ee][Xx]|(にゃん|ニャン){2}|生殖.*)しよ/x
	    if c.affection > 90 && c.tsundere > 10
		post_status("#{to} #{tere}", c.status)
	    else
		resp = rand_item(['それってなあに？',
				  'それってどういうことするの？'])
		post_status("#{resp} #{rt}", c.status)
	    end
	    return true

	when /(一緒|いっしょ)に(寝|ねよう|眠|ねむ|スリーピング)/x
	    if c.affection > 90 && c.tsundere > 10
		resp = rand_item(['やった〜♪',
				  'ご本読んで欲しいなっ',
				  'それじゃご本読んで！'])
	    else
		resp = rand_item(['ひ、一人で眠れるから大丈夫よ',
				  '…何か変なこと期待してない？',
				  '何百年一人で寝ていたと思うの？'])
	    end
	    post_status("#{to} #{resp}", c.status)
	    return true

	when /(もみもみ|揉み揉み)/x, /(ぬっちょ|ぬっぽ|じゅっぽ|じゅぽ){2}/x
	    if c.affection > 70 && c.tsundere > 0
		resp = rand_item(GENERIC_KANJIRU) + tere
		c.uconf[:affection] += dice(1, 6)
		c.uconf[:tsundere] += dice(1, 4)
		post_status("#{to} #{resp}", c.status)
	    else
		resp = rand_item(['ど、どこ触ってるの変態！'] + GENERIC_BATOU)
		c.uconf[:affection] -= dice(1, 6)
		c.uconf[:tsundere] -= dice(1, 4)
		post_status("#{resp} #{rt}", c.status)
	    end
	    return true

	when /(ぺろぺろ|ペロペロ|ﾍﾟﾛﾍﾟﾛ|れろれろ|レロレロ|ﾚﾛﾚﾛ)/x
	    if c.tsundere < 0 && c.text =~ /(靴|ニーソ|ﾆｰｿ|ソックス|ｿｯｸｽ|足|脚)/x
		resp = rand_item(["あら、下僕としてはいい心がけだね",
				  "うわ、ホントになめてるよ… ｷﾓｯ"])
		c.uconf[:affection] += dice(1, 6)
		c.uconf[:tsundere] -= dice(1, 4)
		post_status("#{resp} #{rt}", c.status)
	    elsif c.affection > 70 && c.tsundere > 0
		resp = rand_item(["そ、そんなところキタナイよぉ…"] \
				 + GENERIC_KANJIRU) + tere
		c.uconf[:affection] += dice(1, 6)
		c.uconf[:tsundere] += dice(1, 4)
		post_status("#{to} #{resp}", c.status)
	    else
		resp = rand_item(["な、なに気持ち悪いこと言ってるの！？"] \
				 + GENERIC_BATOU)
		c.uconf[:affection] -= dice(1, 6)
		c.uconf[:tsundere] -= dice(1, 4)
		post_status("#{resp} #{rt}", c.status)
	    end
	    return true

	when /#{FRAN_RE}.*(おいしい|はむはむ|もぐもぐ|モグモグ|ﾓｸﾞﾓｸﾞ|mogmog)/ox
	    # 誤爆防止のため、「フランちゃんのｘｘ」にしか反応しない
	    if c.affection > 70 && c.tsundere > 0
		resp = rand_item(["そ、そんなのキタナイよぉ…",
				  "お腹壊しちゃうよぉ",
				  "そんなものまで食べてくれるなんて嬉しい…"])
		resp += tere
		post_status("#{to} #{resp}", c.status)
	    else
		resp = rand_item(["な、なに気持ち悪いこと言ってるの！？",
				  "そんなもの食べるなんて…"] + GENERIC_BATOU)
		c.uconf[:affection] -= dice(1, 6)
		c.uconf[:tsundere] -= dice(1, 4)
		post_status("#{resp} #{rt}", c.status)
	    end
	    return true
	    
	when /(さわ|触)(って(いい|良い).*(\?|？)|らせて)/x
	    if c.affection > 70 && c.tsundere > 0
		resp = rand_item(['う、うん…',
				  '…ちょっとだけだよ？'])
		c.uconf[:affection] += dice(1, 6)
		c.uconf[:tsundere] += dice(1, 4)
		post_status("#{to} #{resp}", c.status)
	    else
		resp = rand_item(["ダメに決まってるでしょ！"] + GENERIC_BATOU)
		c.uconf[:affection] -= dice(1, 6)
		c.uconf[:tsundere] -= dice(1, 4)
		post_status("#{resp} #{rt}", c.status)
	    end
	    return true

	when /(おしり|お尻|おっぱい|ちっぱい|胸|貧乳|ひんぬー).*/x
	    resp = rand_item(['何考えてるの!?', '最低！'] + GENERIC_BATOU)
	    c.uconf[:affection] -= dice(1, 6)
	    c.uconf[:tsundere] -= dice(1, 4)
	    post_status("#{resp} #{rt}", c.status)
	    return true

	when /(ちゅっちゅ|チュッチュ|ﾁｭｯﾁｭ)/x, /(ちゅっ|チュッ|ﾁｭｯ|ちゅー|チュー|ﾁｭｰ)#{EOL}$/ox
	    if c.affection > 90
		if c.tsundere > 0
		    resp = rand_item(["…ちゅっ♥",
				      "…ちゅっ#{tere}",
				      "#{c.cn}、ちゅっちゅっ♥",
				      "#{c.cn}、ちゅっちゅっ#{tere}",
				      "は、はぅぅ…#{tere}",
				      "こんなところで恥ずかしいよぉ#{tere}",
				      "こんなところで恥ずかしい…#{tere}",
				      "はっ、恥ずかしいっ…#{tere}"])
		else
		    resp = rand_item(["バ、バカッ！…ちゅっ#{tere}",
				      "こんなところじゃ見られちゃうよ…#{tere}"])
		end
		c.uconf[:affection] += dice(1, 6)
		c.uconf[:tsundere] += dice(1, 4)
	    elsif c.affection > 70
		if c.tsundere > 0
		    resp = rand_item(["は、はぅぅ…#{tere}",
				      "こんなところで恥ずかしいよぉ#{tere}",
				      "こんなところで恥ずかしい…#{tere}",
				      "はっ、恥ずかしいっ…#{tere}",
				      "他の人間に見られちゃうよ…#{tere}"])
		else
		    resp = rand_item(["バ、バカッ！#{tere}",
				      "こんなところでできるわけないじゃない#{tere}"])
		end
		c.uconf[:affection] += dice(1, 4)
		c.uconf[:tsundere] += dice(1, 2)
	    else
		if c.tsundere > 0
		    resp = "はいはい、ﾁｭｯﾁｭ♥"
		else
		    me = (c.tsundere < -5) ? "下僕" : "人間"
		    resp = "#{me}のくせになまいきだね… 靴の裏でも舐めなさい"
		end
		c.uconf[:affection] += dice(1, 8)
	    end
	    post_status("#{to} #{resp}", c.status)
	    return true

	when /(パンツ|ぱんつ|ドロワーズ|ドロワース|ブラ|下着)/x
	    if c.tsundere > 0
		resp = "…そんなものに興味あるの？#{tere}"
	    else
		resp = "バ、バッカじゃないの？///"
	    end
	    post_status("#{to} #{resp}", c.status)
	    c.uconf[:affection] -= dice(1, 4)
	    return true

	when /(足|脚|靴|ニーソ|ソックス)(で|こき|コキ|ｺｷ)(して|シて)/x, \
	    /(踏んで|ふんで)(下さい|ください)/x
	    c.uconf[:tsundere] -= dice(2, 6) # ツン方向
	    if c.affection > 90
		if c.tsundere > 0
		    resp = rand_item(["こんなのがいいんだ…#{c.cn}は変態さんだね！",
				      "足でされて感じちゃうなんて#{c.cn}は変態だね",])
		else
		    resp = rand_item(["こんなのがいいんだ…ｷﾓｯ！",
				      "#{c.cn}なんか、足で十分なんだから！",
				      "#{c.cn}みたいな変態は一生這いつくばってればいいのよ",
				      "うわっ、汚いもの飛ばさないでよ！"])
		end
		post_status("#{to} #{resp}", c.status)
	    else
		post_status("うわぁ、変態さんだぁ… #{rt}", c.status)
	    end
	    return true

	when /(は|履|穿)いてない/x
	    c.uconf[:affection] -= dice(1, 4)
	    resp = rand_item(["そ、そんなことないってばっ！",
			      "ちゃんとはいてるってば！",
			      "健康にいいんだよ？#{tere}"])
	    post_status("#{resp} #{rt}", c.status)
	    return true

	when /(膝|ひざ)(枕|まくら)してあげる/x

	when /(膝|ひざ)(枕|まくら)して/x
	    if c.affection > 70 && c.tsundere > 0
		resp = rand_item(["そんなところに押しつけちゃだめぇ",
				  "そんなもぞもぞ動いたらくすぐったいよぉ",
				  "しょ、しょうがないなあ",
				  "しかたないなあ…"]) + tere
	    elsif c.affection > 70 && c.tsundere < 0
		na = 'な、' * (1 + rand(4))
		resp = "#{na}何言ってるの" + tere
	    else
		resp = "眠いなら、ちゃんとベッドで寝た方がいいよ？"
	    end
	    post_status("#{to} #{resp}", c.status)
	    return true

	when /くんかくんか|クンカクンカ|ｸﾝｶｸﾝｶ/x
	    resp = rand_item(GENERIC_BATOU)
	    post_status("#{to} #{resp}", c.status)
	    return true
	end

	# スキンシップ
	candidate = []
	SH_WORD_TO_REACTIONS.each do |x|
	    regex, list = x[0], x[1]
	    if c.text =~ regex
		list.each {|label| candidate += SH_REACTION_TABLE[label]}
	    end
	end
	if candidate.length > 0
	    resp = rand_item(candidate) + tere
	    post_status("#{to} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 12)
	    return true
	end

	return false
    end

    TIYA_ADJ1 = '((かわいい|カワイイ|ｶﾜ.*ｲｲ|可愛い|きれい|綺麗|優しい|やさしい)(ね|よ|お|なあ|ぜ|ZE)?)'
    TIYA_ADJ2 = '((かわゆす|ｶﾜﾕｽ)(なあ)?)'
    TIYA_ADJV = '((好|す)き(です|だ|だよ|だお|だぜ|だZE|っ)?)'
    TIYA_VERB = '(((愛|あい)してる|大切にする)(ね|よ|お|ぜ|ZE)?)'
    TIYA_ALL = "(#{TIYA_ADJ1}|#{TIYA_ADJ2}|#{TIYA_ADJV}|#{TIYA_VERB})"

    # ちやほや
    def tiyahoya(c)
	# まず、自分宛てでなくてもチェック

	# 誤爆しやすいので、「名前+好き」「好き+名前」「好き$」のパターンに限定
	# botへの発言をチェックし、浮気していたら記録
	recipients = c.twit[:r_to] + c.twit[:m_to] + c.twit[:rt_to]
	name = nil
	TOHO_OTHER_BOTS.each_pair do |bot_screen_name, bot_name|
	    name = bot_name if recipients.include?(bot_screen_name)
	end
	name = nil if (FRANBOT_LIST & recipients).size > 0

	if name != nil && c.text =~ /(#{TIYA_ADJV}|#{TIYA_VERB})/ox
	    c.uconf[:uwaki_aite] = name
	    c.uconf[:uwaki_status_id] = c.status.id
	end

	# 自分宛てでない場合はここで処理終了
	return false unless c.to_me

	# フランbotへの発言をチェック
	if c.text =~ /#{FRAN_RE}.*#{TIYA_ALL}/ox \
	    || c.text =~ /#{TIYA_ALL}.*#{FRAN_RE}/ox \
	    || c.text =~ /#{TIYA_ALL}#{EOL}$/ox
	    # 浮気チェック
	    if c.uconf[:uwaki_aite] != ''
		id = c.uconf[:uwaki_status_id]
		url = "http://www.twitter.com/#{c.nm}/status/#{id}"
		target = c.uconf[:uwaki_aite]
		rt = "RT @#{c.nm}: #{c.rawtext}"
		post_status("@#{c.nm} ……ねえ、#{target}にもそんなこと言ってなかった？ #{url} #{rt}")
		c.uconf[:uwaki_aite] = ''
		c.uconf[:uwaki_status_id] = 0
		c.uconf[:affection] -= dice(2, 20)
		c.uconf[:tsundere] -= dice(2, 10)
		return true
	    end

	    # パラメータによって分岐
	    if c.affection > 30
		if c.tsundere < 0
		    resp = rand_item(["おだてりゃいいってものじゃないよ？",
				      "人間の考えることは理解不能ね",
				      "そんなこと言っても、何も出ないんだからね！",
				      "それで口説いているつもりかしら？",
				      "じゃあ、あなたを壊してもいいよね！"])
		    c.uconf[:affection] += dice(1, 6)
		    c.uconf[:tsundere] -= dice(1, 4)
		else
		    resp = rand_item(["あ、ありがとう",
				      "えへへへへ",
				      "なんだか照れるよ…",
				      "なんだか照れるなあ"]) + tere_aa()
		    c.uconf[:affection] += dice(1, 10)
		    c.uconf[:tsundere] += dice(1, 4)
		end
	    else
		resp = rand_item(["きゅ、急にどうしちゃったの？",
				  "とっ、突然何を言い出すのかしら？",
				  "うれしいけど、急に言われても困るよ"]) + tere_aa()
		c.uconf[:affection] += dice(1, 6)
	    end
	    post_status("@#{c.nm} #{resp}", c.status)
	    return true
	end
    end

    # おつきあい
    def kousai(c)
	if c.only_to_me && c.text =~ /((つ|付)き(あ|合)って|交際して|彼女になって|彼氏にして)/x
	    if c.affection > 90
		if c.tsundere > -10
		    resp = "いまさらだなあ♥"
		else
		    resp = "ま、まあ考えてあげてもいいかなっ！"
		end
		c.uconf[:affection] += dice(1, 6)
	    elsif c.affection > 60
		resp = rand_item(['は、はい♥', 'う、うん♥',
				  'そんな…急に…///'])
		c.uconf[:affection] += dice(1, 20)
	    elsif c.affection > 30
		resp = "お友達のままじゃダメ？"
		c.uconf[:affection] += dice(1, 6)
	    else
		resp = "ま、まずはお友達からね" + tere_aa()
		c.uconf[:affection] += dice(1, 4)
	    end
	    post_status("@#{c.nm} #{resp}", c.status)
	    return true
	end
	return false
    end

    # プロポーズ
    def propose(c)
	if c.only_to_me && c.text =~ /(結婚して|結婚しよう|お嫁に|お婿に)/x
	    tere = tere_aa()
	    if c.affection > 97
		if c.tsundere > +10
		    resp = "は、はい…#{tere}"
		elsif c.tsundere < -10
		    resp = "バ、バッカじゃないの…#{tere}"
		else
		    resp = "幻想郷には役所なんてないから、それはちょっと無理かな… でも、気持ちだけもらっとくね♪"
		end
		c.uconf[:affection] += dice(4, 6)
		c.uconf[:tsundere] += dice(1, 6)
		post_status("@#{c.nm} #{resp}", c.status)

	    elsif c.affection > 60
		if c.tsundere > +10
		    resp = "そ、そんなこと言われても困るよ#{tere}"
		elsif c.tsundere < -10
		    na = 'な、' * (1 + rand(4))
		    resp = "#{na}何を言ってるのかしら…#{tere}"
		else
		    resp = "吸血鬼と人間じゃ、ちょっと無理じゃないかなあ… でも、#{c.cn}の気持ちはわかったよ♪"
		end
		c.uconf[:affection] += dice(3, 6)
		c.uconf[:tsundere] += dice(1, 6)
		post_status("@#{c.nm} #{resp}", c.status)

	    else
		resp = rand_item(['そういうことは軽々しく言っちゃダメだと思うよ？',
				  'そういう冗談はちょっとひどくない？'])
		c.uconf[:affection] -= dice(3, 6)
		c.uconf[:tsundere] -= dice(2, 6)
		post_status("@#{c.nm} #{resp}", c.status)
	    end
	    return true
	end
	return false
    end

end
