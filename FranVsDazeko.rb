#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
# $Revision: 1.7 $ $Date: 2010/04/09 18:05:19 $
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    #----------------------------------------------------------------------
    # 魔理沙bot専用の応答処理

    DAZEKO_COMMON = [ # 対魔理沙用の独り言
		     "魔理沙ってキノコ食べると大きくなるの？",
		     "魔理沙のどろわーずmogmog",
		     "魔理沙の魔法見せて欲しいな♪",
		     "私もマスタースパーク撃ってみたい…え、やめとけって？",
		     "ねえねえ♪",
		     "にんてんどうでぃーえすでまりさカートやろうよ！",
		     "にんてんどううぃーでまりさカートやろうよ！",
		    ]

    # 魔理沙bot用のひとり言
    def dazeko_monologue()
	# ポスト規制値に近づいていたら処理終了
	if too_many_posts?(0.2)
	    @logger.warn("suppressed response for dazeko(monologue)")
	    return
	end

	resp = nil
	tere = tere_aa()
	if @gconf[:dazeko_visiting]
	    # 魔理沙が紅魔館に来ているときのひとり言
	    resp = rand_item(["弾幕ごっこしようよ！",
			      "パチュリーのところに遊びに行く？",
			      "遊びに来てくれるのは嬉しいけど、あんまり美鈴をいぢめちゃだめだよ？",
			      "お腹空いてない？",
			      "私の部屋にくる？#{tere}",
			      "ベッドの下覗いちゃダメだからね？",
			      "咲夜にお茶にしてもらおうよ",
			     ] + DAZEKO_COMMON)
	else
	    # 紅魔館に来ていない時はさらに1/32の確率でひとり言
	    resp = rand_item(["魔理沙、遊びに来ないかな…",
			      "ふたまたは良くないと思うよ",
			      "古風な魔女の格好もかわいいね♪",
			      "帽子のリボンかわいいね♪",
			      "魔理沙のスペルカードってかわいい名前多いよね",
			      "まりさかわいいよ かわいいよまりさ",
			      "魔理沙かわいいよ かわいいよ魔理沙",
			      "たまには紅魔館にも来てよね。えっと、その、パチュリーも寂しがってたから…ｺﾞﾆｮｺﾞﾆｮ#{tere}",
			     ] + DAZEKO_COMMON)
	    resp = nil if rand(32) != 0
	end
	post_status("@dazeko #{resp}") if resp != nil
    end

    DAZEKO_MAKENAI = ["今度は負けないよ？",
		      "今度は負けないんだから！",
		      "とか言ってるわりにはよく負けてるよね♪",
		      "禁忌「恋の迷路」をおみまいしてあげるんだから！",
		      "私の「恋の迷路」を抜けてこれるかなあ？",
		     ]
    PREFFERED_MAGIC_SCHOOL = ["範囲攻撃", "精神攻撃", "即死系",
			      "闇属性攻撃", "ネクロマンシー"]

    # 魔理沙への応答
    def respond_for_dazeko_bot(status, uconf)
	c = create_context(status, uconf)
	if c.text =~ /(.*)\[(.*)の家\]$/x
	    body = $1; house = $2
	elsif c.text =~ /(.*)\[博麗神社\]$/x
	    body = $1; house = '博麗神社'
	else
	    body = c.text; house = ''
	end
	rt = "RT @#{DAZEKO_BOT}: #{c.rawtext}"
	tere = tere_aa()

	# ポスト規制値に近づいていたら処理終了
	if too_many_posts?(0.4)
	    @logger.warn("suppressed response for dazeko(dialogue)")
	    return
	end

	# 場所のチェック
	if ['フラン', 'フランドール・スカーレット(bot)'].include?(house)
	    if @gconf[:dazeko_visiting] == false
		post_status("@dazeko 魔理沙いらっしゃい！#{tere}", status)
	    end
	    @gconf[:dazeko_visiting] = true
	else
	    if @gconf[:dazeko_visiting] == true
		post_status("魔理沙いっちゃった… もっといて欲しかったなあ…")
	    end
	    @gconf[:dazeko_visiting] = false
	end

	# ここ以降の処理では、他ユーザー向けの発言には応答しない
	return if c.except_me

	#---------------------------------------------------------------------
	# 1/8の確率で時報に反応
	if body =~ /^\d+時だぜ。/x
	    tere = (rand(4) == 0) ?  tere : '' # 1/4の確率でAAを使う
	    resp = rand_item(["魔理沙、また遊ぼうね#{tere}",
			      "魔理沙、また遊んでくれる？",
			      "また遊びにきてね、魔理沙#{tere}",
			      "また弾幕ごっこしようね、魔理沙#{tere}",
			      "またお茶でも飲みにきてね、魔理沙#{tere}",
			      "お仕事する魔理沙カッコいい…#{tere}",
			      "今度、時報代わりにマスタースパークでも撃ってみたらどうかしら",
			     ])
	    if rand(8) == 0
		post_status("@dazeko 時報お疲れさま♪#{resp}", status)
	    end
	    return
	end

	#---------------------------------------------------------------------
	# 紅魔館にいるときの特殊応答
	if @gconf[:dazeko_visiting]
	    # 施設・部屋
	    case body
	    when /なんか面白い本とかないか？/x
		post_status("それじゃパチュリーのとこに遊びにいこうよ #{rt}", status)

	    when /トイレ/x
		post_status("やだ魔理沙ったら…/// 案内するね ●REC #{rt}", status)

	    when /(ベッドの下にエロ本とか落ちてないかな|面白そうなもんはないかなっと…)/x
		post_status("えっ…お気に入りのフラマリ本見つかったらどうしよう#{tere} #{rt}", status)

		# 食べ物・キッチン
	    when /腹減ったぜ/x
		resp = rand_item(HAKUDAKUEKI) + "でも飲む？"
		post_status("#{resp} #{rt}", status)

	    when /#{WS}*(.*)ってないのか？/x
		post_status("#{$1}？ 咲夜に聞いてみようよ #{rt}", status)

	    when /少女つまみ食い中…/x
		post_status("咲夜に怒られても知らないよ♪ #{rt}", status)

	    when /キッチン借りていいか？/x
		post_status("うん、いいよー♪ あ、でも、パチュリーの実験室のほうがいいかな？ #{rt}", status)

		# その他
	    when /さて…夜もふけたところで物色開始だぜ！/x
		post_status("手癖が悪いのも困ったものね… 私を頂いてもいいのよ#{tere} #{rt}", status)
	    end
	end

	#---------------------------------------------------------------------
	# 一般のケース(時報でも紅魔館滞在中でもない場合)
	case body

	    #======================================== 「かわいい」への反応
	when /な、なんだよその目は/x
	    # 実際には「あ、ありがとう。…な、なんだよ」となるので、順番注意
	    resp = rand_item(["別に？にやにや！", "魔理沙照れてるｗ"])
	    post_status("#{resp} #{rt}", status)
	when /は、恥ずかしいからあんまり褒めるなよ/x
	    post_status("魔理沙照れてる！にやにや！ #{rt}", status)
	when /そういわれるのはどうも慣れないな/x
	    post_status("魔理沙はかわいいよ&hearts; #{rt}", status)
	when /(そうか？|…そう、か？)/x
	    post_status("うんっ&hearts;", status)

	    #======================================== 「かっこいい」への反応
	when /可愛いって言ってくれても…/x
	    post_status("うふふ、魔理沙かわいいよ&hearts; #{rt}", status)
	when /(私だって女の子だぜ…|カッコいい…か…|う…そうか。ありがとう…|悪くはないんだがその)/x
	    resp = rand_item(["魔理沙は同性にもてるタイプだよね…",
			      "だってそうだもん#{tere}"])
	    post_status("#{resp} #{rt}", status)

	    #======================================== 感謝・詰問・許容
	when /へへ、ありがとな。まだまだいけるけどな/x
	    post_status("魔理沙さすがだねっ♪ #{rt}", status)

	when /(ありがと|よろしく|嬉しいぜ)/x
	    post_status("どういたしまして#{tere} #{rt}", status)

	when /(ばかやろう|馬鹿野郎|やめろ|許さん|うるさい)/x
	    post_status("ご、ごめんなさい… #{rt}", status)

	when /うーん、謝られても変な感じなんだが…/x
	    post_status("じゃあ、仲直りねっ #{rt}", status)

	    #======================================== 遊び
	when /ん？遊んで欲しいのか？/x
	    resp = rand_item(["うんっ#{tere}",
			      "うん！ えへへー#{tere}",
			      "うん、フランゲームしようよ！ ポッキーの代わりにフランを両側から食べるの！"])
	    post_status("#{resp} #{rt}", status)

	when /(フランのしたい遊びでいいぜ。|いいな。何して遊ぶ？)/x
	    resp = rand_item(["弾幕ごっこがいい！",
			      "じゃあ性的な遊び！にやにや！",
			      "じゃあ図書館でかくれんぼしようよ！",
			      "じゃあ図書館でどーじんし読もうよ！",
			      "じゃあ図書館でえっちなどーじんし読もうよ！"])
	    post_status("#{resp} #{rt}", status)

	when /(おう、遊んでやってもいいぜ！|あー？仕方ないな、遊んでやるか)/x
	    resp = rand_item(['やったー', 'わーい', '嬉しいな', 'ありがとう'])
	    post_status("#{resp}#{tere} #{rt}", status)

	when /付き合ってやってもいいぜ。/x
	    post_status("やった！魔理沙とおつきあい&hearts; …ってなんだ遊んでくれるだけなのね(´・ω・｀) #{rt}")

	when /(にんてんどうでぃーえすがやりたいぜ)/x
	    post_status("らぶぷらすとかやってみたいよね♪ #{rt}", status)

	    #======================================== 弾幕ごっこ
	when /(こてんぱんにのしてやろう|お、やるか？負けないぜ、
|私に勝負を挑むとは、それなりの覚悟は出来ているんだろうな？)/x
	    resp = rand_item(DAZEKO_MAKENAI)
	    post_status("#{resp} #{rt}")

	when /(ふふふ、落とせるものなら落としてみな|そう簡単には落されないぜ)/x
	    resp = rand_item(DAZEKO_MAKENAI +
			     ["魔理沙を落とす… ｸﾞﾌﾌﾌﾌ#{tere}"])
	    post_status("#{resp} #{rt}", status)

	    #======================================== 紅魔郷キャラ
	when /(吸血鬼の妹のやつに見つかると|悪いやつじゃないんだけどな、フランは。)/x
	    resp = rand_item(['ﾄﾞｷｯ', 'ﾋﾞｸｯ', 'ｷﾞｸｯ'])
	    post_status("#{resp} #{rt}", status)

	when /あいつは機嫌を損ねると恐ろしいことになるからいつも苦労するよ/x
	    resp = "あとで地下室でたっぷりかわいがってあげるね&hearts;"
	    post_status("#{resp} #{rt}", status)

	when /あそこのお嬢様はワガママ過ぎるぜ/x
	    resp = rand_item(['まあ…ね', 'お姉様の悪口言わないで！',
			      'ワガママなのは本当かな…'])
	    post_status("#{resp} #{rt}", status)

	when /門番は大抵寝てるから簡単に忍び込めるぜ。/x
	    resp = rand_item(["あとで美鈴をシメておかなくちゃ…",
			      "なんで普通に入ってこないのよ…",
			      "もっと遊びに来てくれてもいいんだよ？#{tere}"])
	    post_status("#{resp} #{rt}", status)

	    #======================================== 図書館
	when /あそこは本がいっぱいでいいよな|あいつの図書館は面白い本が沢山だぜ/x
	    resp = rand_item(["じゃあ、もっと遊びに来てくれればいいのに…",
			      "もう魔理沙ったら… だったら、紅魔館に住んじゃえばいいのに… な、なんでもないよっ！#{tere}",
			      "パチュリーに怒られちゃうから、本は持っていっちゃだめだよ？",
			      "魔道書とかどーじんしとかいっぱいあるよね"])
	    post_status("#{resp} #{rt}", status)

	when /本は死んだら返すって言ってるだろ/x
	    # パチュリーまたは図書館への反応
	    post_status("そんな悲しいこと言わないでよ… #{rt}", status)

	when /いつも咳してて見ててこっちが辛いぜ/x
	    post_status("いつもほこりっぽいところにいるからね… #{rt}", status)

	when /アリスもたまに見かけるな、あそこに行くと/x
	    # 紅魔館または図書館への反応
	    resp = rand_item(["魔法のお話してるみたいだよ",
			      "本のお話してたよ。どーじんしとか言ってたかな？",
			      "魔理沙のお話してたよ。ぱちゅありまりさいこーとか言ってた",
			      "ケンカしてたっけ。ぱちゅありととありまりはどっちが良いかって…"
			     ])
	    post_status("パチュリーと#{resp} #{rt}", status)

	when /だから本は借りてるだけで、盗んでるわけじゃないぜ。/x
	    post_status("じゃあ、ちゃんと返しにいかないとだめだよ？ #{rt}", status)

	when /何読んでるんだ？私にも見せろよ/x
	    # フォロー案内に対して
	    post_status("「とりせつ」とかいうらしいよ？ #{rt}", status)

	    #======================================== 移動・訪問
	when /ただいま/x
	    post_status("おかえりなさい！ #{rt}", status)

	when /(今度また遊びに来るぜ|今度遊びに行くぜ|近いうちに絶対行くぜ)/x
	    post_status("うん、待ってるからね#{tere} #{rt}", status)

	    #======================================== 好意
	when /お前(とは気が合うぜ|に言われると照れる)/x
	    resp = rand_item(['えへへへへ', 'うふふ'])
	    post_status("#{resp}&hearts; #{rt}", status)

	when /私も好きだぜ。…い、一応言っとくが、深い意味はないからな？/x
	    resp = rand_item(['ｷﾀｷﾀｷﾀｷﾀ━━━(ﾟ∀ﾟ≡(ﾟ∀ﾟ≡ﾟ∀ﾟ)≡ﾟ∀ﾟ)━━━━!!',
			      '私も魔理沙が好きだよ！',
			      '私も魔理沙が好きだよ！(性的な意味で)'])
	    post_status("#{resp} #{rt}", status)

	    #======================================== 食べ物
	when /(聞いてたら喉が渇いてきたぜ…|咲夜の淹れる紅茶は一度飲む価値アリだぜ)/x
	    post_status("いつでもお茶を飲みに遊びに来てね&hearts; #{rt}")

	when /私は緑茶が好きだな/x
	    # お茶に反応
	    post_status("本当に和風党なのね #{rt}")

	when /とか言って、嫌らしい事考えてるわけじゃないよ/x
	    # キノコで大きくなるへの反応
	    post_status("ｷﾞｸｯ… な、なんのことかなっ？ #{rt}", status)

	when /きのこだったら、魔法の森によく生えてるぜ。/x
	    post_status("食べても大丈夫なきのこなの？ #{rt}", status)

	    #======================================== 魔法
	when /のために何か面白い魔法を考えておくぜ/x
	    resp = "ありがとう#{tere} 楽しみにしてるね"
	    post_status("#{resp} #{rt}", status)
	when /とっておきの魔法見せてやろうか/x
	    post_status("やった！見せて見せて&hearts; #{rt}", status)
	when /どんな魔法が好きだ？/x
	    resp = rand_item(PREFFERED_MAGIC_SCHOOL) +
		"魔法が好き" + rand_item(["！", "かな", "かな？"])
	    post_status("#{resp} #{rt}", status)
	when /(.*)魔法なら得意だぜ/x
	    post_status("そうだったんだ…ちょっと意外だな #{rt}", status)

	    #======================================== その他の会話
	when /やれやれ、.*は子供だな/x
	    resp = rand_item(['魔理沙よりずっと年上だよっ！',
			      'ぶー、子ども扱いしないでよo(｀ω´*)oﾌﾟﾝｽｶﾌﾟﾝｽｶ!!',
			      'じゃあ、オトナのおつきあい…しましょ？' + tere])
	    post_status("#{resp} #{rt}")

	when /よし、じゃあ占いをしてやろう！何占いがお望みだ？/x
	    resp = rand_item(['お願いする♪…魔理沙との…ｺﾞﾆｮｺﾞﾆｮ' + tere,
			      'お願いするね&hearts;'])
	    post_status("@dazeko じゃあ、恋占いを#{resp}", status)

	when /うーむ暇だな/x
	    resp = rand_item(["じゃあかまってよ〜&hearts;",
			      "じゃあ私と遊んでよ",
			      "じゃあ弾幕ごっこしようよ"])
	    post_status("#{resp} #{rt}", status)

	end
    end
end
