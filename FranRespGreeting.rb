#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    # 挨拶処理
    # - 時刻の挨拶などは特に自分あてでなくても応答する
    # - しかし、他の人に同じ挨拶を繰り返す場合があるので、
    #   同じ挨拶を短時間で繰り返さないようにする
    # - 挨拶と同じ字句が含まれる挨拶返しがある
    #   (「おやすみ」に対して「おやすみあり」など)ため、
    # - 挨拶返しと感謝の挨拶がかぶるおそれがある(「おやすみありがとう」など)
    # - 挨拶返しには応答しない(「おやあり」など)が、処理済みとして扱う

    # 挨拶1 (時刻)
    # 他ユーザー向けの発言には応答しない
    # おはようとおやすみは頻度高いので、できるだけバリエーション増やしていく
    def greeting1(c)
	return false if c.except_me
	return false if c.text =~ /あり(がと|です|でした)/x

	case c.text
	when /(おはよ(う|ー)?|ｵﾊﾖ(ｳ|ｰ)?|お早う|おはっす
|むくり|mkr|ｍｋｒ|えむけーあーる)/x
	    return true if @curr_time < c.uconf[:last_greeting_morning] + 600
	    c.uconf[:last_greeting_morning] = @curr_time

	    resp = rand_item(["おはよう、#{c.cn}！", "おはよう、#{c.cn}♪",
			      "#{c.cn}、おはよう！", "#{c.cn}、おはよう♪", 
			      "#{c.cn}、おはよっ♪", 
			      "おはよう…まだ眠いよぉ",
			      "おはようじょ♪",
			      "おはようむ♪",
			      "おはよう、#{c.cn}！私はこれから寝るところだけどね///",
			      "おはよう！#{c.cn}はこんな時間に起きるんだ。人間って大変だね"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 6)
	    return true

	when /((こんにち|こんち|こにゃにゃち)(は|わ)|こんちゃ)/x
	    return true if @curr_time < c.uconf[:last_greeting_day] + 600
	    c.uconf[:last_greeting_day] = @curr_time

	    if rand(4) == 0
		post_status("@#{c.nm} ろりこんにちは！", c.status)
	    else
		post_status("@#{c.nm} こんにちは、#{c.cn}！", c.status)
	    end
	    c.uconf[:affection] += dice(1, 6)
	    return true

	when /(こんばん(は|わ|にゃ)|こばにゃ)/x
	    return true if @curr_time < c.uconf[:last_greeting_evening] + 600
	    c.uconf[:last_greeting_evening] = @curr_time

	    case c.mop
	    when 0, 29 # 新月
		c.uconf[:affection] += dice(2, 6)
		resp = "こんばんは。今日は月が冥い夜だね"
	    when 13, 14 # 満月
		c.uconf[:affection] += dice(1, 4)
		case rand(5)
		when 0
		    resp = "こんばんは…満月が近くてなんだか落ち着かないね"
		when 1
		    resp = "こんばんは…今日はなんだか熱っぽいかも"
		when 2
		    resp = "N'ak kode tihs ot tuyg... あ、こんばんは！気がつかなくてごめんね"
		else
		    resp = "こんばんは。そろそろ満月かな"
		end

	    when 15 # 十六夜(いざよい)
		c.uconf[:affection] += dice(1, 6)
		resp = "こんばんは。今晩は十六夜かな。咲夜のことじゃないよ？"
	    else # その他
		c.uconf[:affection] += dice(1, 6)
		case rand(4)
		when 0
		    resp = "ろりこんばんは！"
		else
		    resp = "こんばんは、#{c.cn}！"
		end
	    end
	    post_status("@#{c.nm} #{resp}", c.status)
	    return true

	when /(おやすみ|お休み|ｵﾔｽﾐ)/x
	    return true if @curr_time < c.uconf[:last_greeting_night] + 600
	    c.uconf[:last_greeting_night] = @curr_time

	    resp = rand_item(["また明日ね、#{c.cn}！",
			      "#{c.cn}、また明日ね♪",
			      "#{c.cn}、いい夢見てね！",
			      "#{c.cn}、またあとで遊ぼうね！おやすみ！",
			      "…いっしょに寝ていい？…なーんてウソだよ☆",
			      "うぅ、さびしいなあ…おやすみなさい！",
			      "えーっ、夜はこれからなのに… おやすみなさい！",
			      "おやすみなさい、#{c.cn}！",
			      "おやすみなさい、#{c.cn}♪"])

	    # 好感度が高い場合は、1/10の確率で反応上書き
	    if c.affection > 70 && c.tsundere > 0 && rand(10) == 0
		tere = tere_aa()
		resp = rand_item(["おやすみのちゅー…してほしいな#{tere}",
				  "いっしょにすりーぴんぐしてほしいな#{tere} http://www.nicovideo.jp/watch/sm9443005"])
		#【ロリ度UP】フランといっしょにすりーぴんぐをアテレコ【してみた】
	    end

	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 6)
	    return true
	end

	return false
    end

    MEAL1_RE = '((食事|食べ|たべ|飯)に?(行|い)っ?て|食事して|食べて|たべて|飯って|メシって|食って)'
    MEAL2_RE = '(飯|めし|メシ)'

    BATH1_RE = '((風呂|ふろ|フロ|シャワー?|ｼｬﾜｰ?|ほか)(って|に?(行|い)っ?て|に?(入|はい)って))'
    BATH2_RE = '(風呂|ふろ|フロ|シャワー?|ｼｬﾜｰ?|ほか)'

    # 挨拶2 (出発/帰宅/風呂/食事)
    # 他ユーザー向けの発言には応答しない
    def greeting2(c)
	return false if c.except_me
	return false if c.text =~ /あり(がと|です|でした)/x

	# 食事
	case c.text
	when /(#{MEAL1_RE}(きた|きました))|(#{MEAL2_RE}った)/ox
	    return true if @curr_time < c.uconf[:last_greeting_meal] + 600
	    c.uconf[:last_greeting_meal] = @curr_time

	    resp = rand_item(["#{c.cn}、おかえりなさ〜い"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 6)
	    return true

	when /(#{MEAL1_RE}(くる|きま))|(#{MEAL2_RE}る)/ox
	    return true if @curr_time < c.uconf[:last_greeting_meal] + 600
	    c.uconf[:last_greeting_meal] = @curr_time

	    resp = rand_item(["もう食事の時間かあ。私もお腹すいたなあ",
			      "もう食事の時間かあ。#{c.cn}、いってらっしゃい！",
			      "もう食事の時間かあ。咲夜に何か作ってもらおうかな",
			      "ちょっと待って！#{c.cn}は私が食べるんだから行っちゃダメ！"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 6)
	    return true

	    # 風呂・シャワー
	when /(#{BATH1_RE}(きた|きました))|(#{BATH2_RE}った)|(ほかいま)/ox
	    return true if @curr_time < c.uconf[:last_greeting_bath] + 600
	    c.uconf[:last_greeting_bath] = @curr_time

	    resp = rand_item(['おかえり♪', 'おかえり〜♪',
			      'ほかえり♪', 'ほかえり〜♪'])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 6)
	    return true

	when /(#{BATH1_RE}(くる|きま))|(#{BATH2_RE}る)/ox
	    return true if @curr_time < c.uconf[:last_greeting_bath] + 600
	    c.uconf[:last_greeting_bath] = @curr_time

	    resp = rand_item(["@#{c.nm} いってらっしゃーい",
			      "@#{c.nm} いってら〜♪",
			      "@#{c.nm} ほかってら♪",
			      "@#{c.nm} ほかてら〜♪",
			      "●REC RT @#{c.nm}: #{c.rawtext}",
			      "ほかてら〜♪ ●REC RT @#{c.nm}: #{c.rawtext}"])
	    post_status(resp, c.status)
	    c.uconf[:affection] += dice(1, 6)
	    return true

	# 外出: 風呂や食事の挨拶と誤爆しやすいので最後に判定する
	when /((い|行|逝)っ?てきま(あ|っ|ー|〜|～)*す)/x
	    return true if @curr_time < c.uconf[:last_greeting_visit] + 600
	    c.uconf[:last_greeting_visit] = @curr_time

	    resp = rand_item(["#{c.cn}、いってらっしゃい！",
			      "#{c.cn}、いってらっしゃい！今日もがんばってね！",
			      "#{c.cn}、いってらっしゃい！帰ったら遊ぼうね！",
			      "#{c.cn}、いってらっしゃい！早く帰ってきてね！",])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 6)
	    return true

	# 帰宅
	when /ただい(魔理沙|まりさ|まりしゃ|マスター|ますたー|マーガトロイド|まーがとろいど)/x
	    return true if @curr_time < c.uconf[:last_greeting_home] + 600
	    c.uconf[:last_greeting_home] = @curr_time

	    resp = rand_item(['おかえーりん！',
			      'おかえりんのすけ！',
			      'おかえりりーほわいと！',
			      'おかえりぐる！'])
	    post_status("@#{c.nm} #{c.cn}、#{resp}", c.status)
	    return true

	when /(ただい(ま|も)|たらい(ま|も)|(帰宅|きたく)(った|〜)
|(帰宅|きたく)$)/x
	    return true if @curr_time < c.uconf[:last_greeting_home] + 600
	    c.uconf[:last_greeting_home] = @curr_time

	    tere = tere_aa()
	    if rand(10) == 0
		resp = "弾幕にする？って、なんでそんなガッカリした顔してるの？"
		if c.affection > 70 && c.tsundere > 0 && rand(3) == 0
		    resp = rand_item(["私にする？#{tere}",
				      "私に食べられる？",
				      "私と遊ぶ？#{tere}"])
		end
		post_status("@#{c.nm} おかえりなさい！ご飯にする？お風呂にする？それとも…", c.status)
		post_status("@#{c.nm} #{resp}", c.status)
	    else
		resp = rand_item(["#{c.cn}、おかえりなさい！",
				  "#{c.cn}、おかえりなさい！早く遊ぼ！",
				  "わーい、#{c.cn}おかえり！"])
		if c.affection > 70 && c.tsundere > 0 && rand(10) == 0
		    resp = rand_item(["#{c.cn}、おかえりなさい！さびしかったんだからね#{tere}",
				      "#{c.cn}、おかえりなさい！ずっと待ってたんだからねっ#{tere}"])
		end
		post_status("@#{c.nm} #{resp}", c.status)
	    end

	    c.uconf[:affection] += dice(1, 6)
	    return true
	end

	return false
    end

    # 挨拶4(時候の挨拶)
    # 明示的にbotに話しかける必要あり
    # 「今年もよろしく」はgreeting4()でもひっかかってしまうので、先に判定する
    def greeting3(c)
	return false unless c.to_me

	curr = @curr_obj
	case c.text
	when /(メリー?クリ|ﾒﾘｰ?ｸﾘ|[Mm]erry (Christ|christ|X'?)mas)/x
	    if curr.month == 12 && curr.mday <= 25
		resp = rand_item(["うー、クリスマス爆発しろ！",
				  "私が悪魔の妹だって知って言ってるの？"])
		post_status("@#{c.nm} #{resp}", c.status)
		return true
	    end

	when /(あ|明)け(まして、?)?おめ|今年もよろ/x
	    if curr.month == 1 && curr.mday >= 1 && curr.mday <= 7
		resp = rand_item(["", # 空文字列
				  "「お年玉」っていうものをもらえるって聞いたんけど…(ｷﾗｷﾗ)"])
		post_status("@#{c.nm} あけましておめでとう！#{resp}", c.status)
		return true
	    end
	end

	return false
    end

    # 挨拶4(感謝・謝罪・よろしく)
    def greeting4(c)
	return false unless c.to_me

	case c.text
	when /(おは(よう?)?|おや(すみ)?|てら|おか(えり)?)(リプ)?(あり|サンクス|thank|thx)/ix
	    # 挨拶返し: 応答しないが、処理済みとして扱う
	    return true

	when /(ありがと|thank|thx)/ix
	    # TODO: ありがとんを入れたいが、機種依存文字
	    if c.tsundere < 0
		resp = "た、ただの気まぐれなんだからね！"
		resp += tere_aa() if c.affection > 40
		post_status("@#{c.nm} #{resp}", c.status)
	    else
		post_status("@#{c.nm} どういたしまして、#{c.cn}！", c.status)
	    end
	    c.uconf[:affection] += dice(2, 6)
	    return true

	when /(ごめん|ゴメン)/x
	    if c.tsundere < -5
		resp = rand_item(["絶対に許早苗", "絶対に許さないよ"])
	    elsif c.tsundere < 5
		resp = rand_item(["しょうがないなあ", "まあいいけどね"])
	    else
		resp = rand_item(["え、気にしないでいいよ♪", "別にいいよ♪"])
	    end
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(2, 6)
	    return true

	when /(よろしく|よろぴく|よろぴこ)/x
	    post_status("@#{c.nm} こちらこそよろしくね♪", c.status)
	    c.uconf[:affection] += dice(1, 12)
	    return true

	when /(お|御)(かえ|帰)り|ｵｶｴﾘ|(お|御)(つか|疲)れ|(ご|御)(くろう|苦労)/x
	    resp = rand_item(["ただいまっ♪",
			      "おかあり♪",
			      "遅くなっちゃってごめんね",
			      "おまたせ"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 12)
	    return true

	when /(お|御)(めでと|目出度)(う|ー)?/x
	    resp = 'ありがとう！'
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(1, 12)
	    return true

	end

	return false
    end
end
