#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    # なめる
    def lick(c)
	if c.to_me && c.text =~ /(.*?)(を|、)?(なめて|舐めて)/x
	    target = $1

	    # 人称変更処理
	    if target =~ /(.*)#{FIRST_PERSON}(.*)/ox
		target = "#{$1}#{c.cn}#{$3}"
	    end

	    resp = "#{target}をなめればいいの？…"
	    resp += rand_item(["うえ、変な味;;",
			       "なにこれニガーイ;;",
			       "なんかしょっぱいなあ",
			       "甘くておいしいね！",
			       "うん、おいしい♥"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    return true
	end
	return false
    end

    HAKUDAKUEKI = ['カルピス原液', '飲むヨーグルト', 'ケフィア',
		   '甘酒', 'とろろ汁', 'バリウム造影剤', '練乳']

    # 食べる・飲む
    # 誤爆が多いので、間投詞や記号まで含めて判定する
    #  - たべて！・のんで！: 要求
    #  - たべて(も)いい(の)よ・のんで(も)いい(の)よ: 許可
    #  - たべてください・のんでください: 依頼
    #  - たべてみて？・のんでみて？: 依頼＋試行（形式動詞）
    #  - たべてみる？・のんでみる？: 疑問＋試行（形式動詞）
    #  - たべない？・のまない？: 勧誘＋疑問
    #  - たべる？・のむ？: 疑問
    #  - たべようよ・のもうよ: 勧誘
    def eat_drink(c)
	return true if c.to_me && !c.only_to_me
	return false unless c.only_to_me

	food = nil; action = nil
	case c.text
	when /(.*?)(を|、)?(食|た)
(べて(！|!)|べても?いいの?よ|べて(くだ|下)さい
|(べてみて|べてみる|べない|べてみない|べる)(？|\?)
|べようよ)/x
	    food = $1; action = :eat

	when /(.*?)(を|、)?(飲|呑|の)
(んで(！|!)|んでも?いいの?よ|んで(くだ|下)さい
|(んでみて|んでみる|まない|んでみない|む)(？|\?)
|もうよ)/x
	    food = $1; action = :drink

	when /(.*?)(を|、)?(吸|す)
(って(！|!)|っても?いいの?よ|って(くだ|下)さい
|(ってみて|ってみる|わない|ってみない|う)(？|\?)
|おうよ)/x
	    food = $1; action = :drink
	end
	return false if food == nil

	# 人称の変化
	if food =~ /(.*)#{FIRST_PERSON}(.*)/ox
	    food = "#{$1}#{c.cn}#{$3}"
	end

	case food
	when /血/x
	    if c.mop >= 12 && c.mop <= 16
		post_status("@#{c.nm} ゴクリ… じゃあ遠慮なくもらうねっ", c.status)
		c.uconf[:affection] += dice(4, 12)
		c.uconf[:tsundere] += dice(2, 12)
	    else
		post_status("@#{c.nm} 今はあまりお腹空いてないかな", c.status)
		c.uconf[:affection] += dice(2, 4)
		c.uconf[:tsundere] += dice(1, 4)
	    end

	when /(酒|アルコール|ワイン|ビール|ウィスキー|酎ハイ|割り)/x
	    resp = "私を酔わせてどうするつもりかしら？"
	    resp += "でも、付き合ってあげるよ" if c.affection > 20
	    resp += tere_aa() if c.affection > 60
	    post_status("@#{c.nm} #{resp}", c.status)

	when /(聖水)/x, /((煎|炒)(り|った)?大?豆)/x
	    post_status("@#{c.nm} 何のいやがらせかしら？", c.status)
	    c.uconf[:affection] -= dice(2, 6)
	    c.uconf[:tsundere] -= dice(1, 6)

	when /(白濁液|白.*液体|体液)/x
	    if c.affection > 90
		resp = rand_item(["…いったい何を飲まそうとしてたのかしら？ RT @#{c.nm}: #{c.rawtext}",
				  "@#{c.nm} うえ、変な味;;",
				  "@#{c.nm} うえ、なんか生臭いよ…",
				  "@#{c.nm} おいしい♪"])
		post_status(resp, c.status)
	    else
		food = rand_item(HAKUDAKUEKI)
		post_status("@#{c.nm} #{food}おいしいね！", c.status)
		c.uconf[:affection] += dice(2, 4)
		c.uconf[:tsundere] += dice(1, 4)
	    end

	when /白ドロリッチ/x
	    post_status("@#{c.nm} 何それ？ ドロリッチはコーヒーがいいな", c.status)

	when /(媚薬|睡眠薬|催淫)/x
	    post_status("@#{c.nm} 私人間じゃないから、そんなもの効かないよ♪", c.status)

	when /(にんにく|ニンニク|大蒜|餃子|ペペロンチーノ|ハートチップル)/x
	    post_status("@#{c.nm} 私吸血鬼だし、にんにく臭いのはちょっと…", c.status)

	when /(お菓子|スイーツ
|チョコ|マシュマロ|ましまろ|キャンディー
|ケーキ|クーヘン|トルテ|スフレ|パイ|タルト|クッキー|サブレ|ビスケット|ゴーフル
|プリン|ムース|ババロア|クレームブリュレ|シュークリーム
|ドロリッチ|ホワイトロリータ)/x
	    resp = rand_item(["わーい、#{food}は大好物なんだ。ありがとう！",
			      "#{food}大好き！ありがとう！",
			      "#{food}は大好きだよ♪いっしょに食べようよ"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(4, 6)
	    c.uconf[:tsundere] += dice(2, 6)

	when /あそこ/x
	    post_status("それってなあに？ RT @#{c.nm}: #{c.rawtext}", c.status)
	    c.uconf[:affection] -= dice(2, 6)
	    c.uconf[:tsundere] -= dice(1, 6)

	when /明治フラン/x
	    post_status("@#{c.nm} 大好きだけど、共食い…ってことはないよね？", c.status)
	    c.uconf[:affection] += dice(4, 6)
	    c.uconf[:tsundere] += dice(2, 6)

	when /#{FRAN_RE}/ox
	    case rand(2)
	    when 0
		action = (action == :drink) ? '飲め' : '食べられ'
		resp = "そ、そんなもの#{action}ないでしょ…"
	    when 1
		action = (action == :drink) ? '飲む' : '食べる'
		resp = "どういう意味で#{action}のかしら"
	    end
	    resp += tere_aa()
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(4, 6)
	    c.uconf[:tsundere] += dice(2, 6)

	else
	    if action == :drink
		action = "飲もう"; hungry = "喉かわいちゃった"
	    else
		action = "食べよう"; hungry = "お腹すいちゃった"
	    end
	    resp = rand_item(["ありがとう！いっしょに#{food}#{action}よ",
			      "うん！じゃあ、いっしょに#{food}#{action}よ",
			      "#{hungry}…いっしょに#{food}#{action}よ"])
	    post_status("@#{c.nm} #{resp}", c.status)
	    c.uconf[:affection] += dice(2, 6)
	    c.uconf[:tsundere] += dice(1, 6)
	end
	return true
    end
end
