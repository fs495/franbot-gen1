#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    # メインルーチンの終わりの処理
    def event_end_of_main_routine(c)
	return false
    end

    # ユーザからの回答をチェック
    def event_check_answer(c)
	return false
    end
end
