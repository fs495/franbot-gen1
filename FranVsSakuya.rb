#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    #----------------------------------------------------------------------
    # 咲夜さんbot専用の応答処理
    def respond_for_sakuya_bot(status, uconf)
	rt = "RT @#{SAKUYA_BOT}: #{status.text}"
	tere = tere_aa()

	#------------------------------------------------------------
	# 無条件で応答する処理をまず実行

	#------------------------------------------------------------
	# ポスト規制値に近づいていたら処理終了
	if too_many_posts?(0.3)
	    @logger.warn("suppressed response for SakuyasanBOT")
	    return
	end

	case status.text
	when '大図書館のほうが騒がしいわ、また白黒魔女が騒いでるのかしら'
	    resp = rand_item(["魔理沙来てるの！？",
			      "魔理沙っ？"])
	    post_status("#{resp} #{rt}", status)
	    return

	when 'みなさん、紅茶が入りましたよ'
	    resp = rand_item(["ありがとー",
			      "咲夜、おつかれさま♪"])
	    post_status("#{resp} #{rt}", status)
	    return

	when '妹様と弾幕ごっこをしますわ。久しぶりに腕がなります'
	    resp = rand_item(["負けないんだからねっ♪",
			      "手加減はなしだからね♪",
			      "負けたら罰ゲームで犬耳の刑だからねっ♪",
			      "負けたら罰ゲームで首輪の刑だからねっ♪",])
	    post_status("#{resp} #{rt}", status)
	    return

	when 'いじめられたいですって？何を言ってるんですか。'
	    resp = rand_item(["いろんな変態がいるよね…",
			      "私に踏まれたいって人間も多いんだけど、似たようなものかなあ…",
			      "人間って変態が多いね… 咲夜は違うよね？",
			      "人間って変態が多いね。咲夜も含めてね…"])
	    post_status("#{resp} #{rt}", status)
	    return

	when '妹様はとてもかわいらしいですよ。'
	    resp = rand_item(["えへへー",
			      "照れるなっ",
			      "咲夜だいすきっ"]) + tere
	    post_status("#{resp} #{rt}", status)
	    return

	when '22時です。妹様が起きてきたようですわ'
	    resp = rand_item(["おはよー♪",
			      "咲夜おはよっ♪",
			      "うー、まだ眠いよぉ…"])
	    post_status("#{resp} #{rt}", status)
	    return

	when '21時です。パチュリー様がお風呂に入るようですわ。'
	    resp = rand_item(["ｶﾞﾀｯ",
			      "●REC"])
	    post_status("#{resp} #{rt}", status)
	    return

	when '15時です。おやつを用意しましたよ。'
	    resp = rand_item(["わーい♪",
			      "おやつなあに？",
			      "お姉様と一緒にたべるー♪"])
	    post_status("#{resp} #{rt}", status)
	    return

	when '10時です。休憩に紅茶は如何ですか？'
	    resp = rand_item(["うん、ありがとう咲夜♪",
			      "頂こうかな♪",
			      "うん、頂くね咲夜♪"])
	    post_status("#{resp} #{rt}", status)
	    return
	end
    end

end
