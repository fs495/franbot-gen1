#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

require 'TwitterBot.rb'
require 'FranConsts.rb'
require 'FranMonologue.rb'
require 'FranRespAdmin.rb'
require 'FranRespFood.rb'
require 'FranRespGreeting.rb'
require 'FranRespMisc.rb'
require 'FranRespNeta.rb'
require 'FranRespPlay.rb'
require 'FranVsOurselves.rb'
require 'FranVsTeruyo.rb'

require 'ExtraNull.rb'

class FranBot < TwitterBot
    #----------------------------------------------------------------------
    # 初期化と設定読み込み

    attr_reader :botname, :instance, :bh

    def initialize(argv, basedir, botname)
	super(argv, basedir)
	@botname = botname.freeze
	@instance = FRANBOT_LIST.index(botname)
	@bh = "^#{WS}*@#{botname}#{WS}+".freeze
    end

    def load_global_config()
	conf = super()
	conf.transaction do
	    set_if_unset(conf, :shindan_maker, [])
	    conf.commit()
	end
	return conf
    end

    # ユーザごとのパラメータをロードする
    # パラメータが設定されてなければ初期値を設定し、
    # また上下限値での値制限も実施する
    def load_user_config(user)
	file = "#{@basedir}/../data_shared/#{user.screen_name}.yaml"
	conf = YAML::Store.new(file)
	conf.transaction do
	    set_if_unset(conf, :affection, INITIAL_AFFECTION)
	    set_if_unset(conf, :tsundere, INITIAL_TSUNDERE)
	    limit_value(conf, :affection, 0, 100)
	    limit_value(conf, :tsundere, -25, +25)

	    set_if_unset(conf, :nickname, [])
	    if conf[:nickname][instance] == nil
		conf[:nickname][instance] = user.name
	    end

	    [:last_user_talk_at, # bot -> user
	     :last_bot_talk_at,  # user -> bot
	     :last_any_talk_at,  # user -> any
	     :last_greeting_morning, :last_greeting_day,
	     :last_greeting_evening, :last_greeting_night,
	     :last_greeting_meal, :last_greeting_bath,
	     :last_greeting_visit, :last_greeting_home].each do |key|
		set_if_unset(conf, key, 0)
	    end

	    set_if_unset(conf, :uwaki_aite, '')
	    set_if_unset(conf, :uwaki_status_id, 0)
	    set_if_unset(conf, :bomb_history, [])
	    set_if_unset(conf, :mew_history, [])
	    set_if_unset(conf, :ufufu_history, [])

	    conf.commit()
	end
	return conf
    end

    #----------------------------------------------------------------------
    # mentionへの反応処理

    # botへの発言を処理する (botがフォローしていないユーザからの発言含む)
    def respond_for_mention(status, uconf)
	# bot(自分自身含む)の発言には一切反応しない
	c = create_context(status, uconf)
	return if FRANBOT_LIST.include?(c.nm)
	return if RELATED_BOTS.include?(c.nm)

	# すでにbotがフォローしている場合は反応しない
	return if is_friend(status.user)

	# フォロー要求を処理する
	resp = nil
	if c.text =~ /^((f|F)ollowして|フォローして)/x
	    resp = "@#{c.nm} #{c.cn}をフォローしたよ。よろしくね♪"
	elsif c.text =~ /^お?友達に(なって|なろう)/x
	    resp = "@#{c.nm} #{c.cn}とお友達になったよ。よろしくね♪"
	end
	if c.to_me && resp != nil
	    if add_friend(c.nm) == nil
		resp = "@#{c.nm} あ、あれっ？フォロー追加に失敗してるかも… 10分くらい待ってフォローされてなければ、もう一回試してね"
	    end
	    post_status(resp, status)

	    # 次回以降のtimeline処理をこのステータス以降から開始するようにする
	    if @gconf[:timeline_last_id] < status.id
		@gconf[:timeline_last_id] = status.id
	    end
	    return
	end

	# update制限に引っかかっていないか調べる
	if too_many_posts?()
	    @logger.warn("suppressed response for mention")
	    return
	end

	# botへの言及があった場合
	if c.to_me
	    post_status("@#{c.nm} あなたは遊び相手になってくれる人間かしら？私は外に出してもらえないから #{OHP} の手順でお友達になってくれるとうれしいな♪", status)
	    return
	end
    end

    #----------------------------------------------------------------------

    # timelineへの反応処理
    # botがフォローするユーザーの発言を処理する
    def respond_for_timeline(status, uconf)
	# update制限に引っかかっていないか調べる
	if too_many_posts?() && status.user.screen_name != @admin
	    @logger.warn("suppressed response for timeline")
	    return
	end

	# 発言者が自分たち自身の処理
	if FRANBOT_LIST.include?(status.user.screen_name)
	    ourselves(status, uconf)
	    return
	end
	# 特別応答処理
	case status.user.screen_name
	when 'remilia_bot'
	    #respond_for_remilia_bot(status, uconf)
	    return
	#when SAKUYA_BOT
	    #respond_for_sakuya_bot(status, uconf)
	    #return
	when 'Komeiji_Koishi'
	    #respond_for_koishi_bot(status, uconf)
	    #return
	when TERUYO_BOT
	    respond_for_teruyo_bot(status, uconf)
	    return
	end
	# 無視処理
	return if RELATED_BOTS.include?(status.user.screen_name)
	return if status.source =~ /twittbot\.net/
	# 一般処理
	respond_for_generic_timeline(status, uconf)
    end

    def respond_for_generic_timeline(status, uconf)
	c = create_context(status, uconf)

	# 条件式から呼び出される各々の関数は、
	# 「bot向けの発言であって、かつそれを処理した」ならtrueを返す
	if unfollow_request(c) \
	    || administration(c) \
	    || event_check_answer(c) \
	    || yobina_henkou(c) \
	    || greeting1(c) \
	    || greeting2(c) \
	    || greeting3(c) \
	    || greeting4(c) \
	    || bakuhatsu(c) \
	    || neta(c) \
	    || asobi(c) \
	    || sekuhara(c) \
	    || tiyahoya(c) \
	    || kousai(c) \
	    || propose(c) \
	    || lick(c) \
	    || eat_drink(c) \
	    || fortune_telling(c) \
	    || give_something(c) \
	    || music_se(c) \
	    || benkyo(c) \
	    || unknown_word_to_bot(c)
	    # bot向けの発言
	    uconf[:last_bot_talk_at] = @curr_time # user -> bot
	    uconf[:last_user_talk_at] = @curr_time # bot -> user
	else
	    # bot向けではなかった発言
	    if user_monologue(c)
		uconf[:last_user_talk_at] = @curr_time # bot -> user
	    end
	end

	# イベント用の応答処理(通常応答とは独立に反応)
	if event_end_of_main_routine(c)
	    uconf[:last_bot_talk_at] = @curr_time # user -> bot
	    uconf[:last_user_talk_at] = @curr_time # bot -> user
	end

	# ユーザの最終発言時刻を記録
	uconf[:last_any_talk_at] = @curr_time # user -> any
    end

    #----------------------------------------------------------------------
    # リプライに関するコンテクスト

    class ReplyContext
	attr_accessor :status, :nm, :rawtext
	attr_accessor :uconf, :cn
	attr_accessor :twit, :text, :except_me, :to_me, :only_to_me
	attr_accessor :mop, :affection, :tsundere
    end

    # ReplyContextを作成するファクトリメソッド
    # trim_rt, moon_phaseなどのメソッドがReplyContextクラスで使えないため
    # この中で設定する
    def create_context(status, uconf)
	c = ReplyContext.new()

	c.status = status
	c.nm = status.user.screen_name
	c.rawtext = status.text

	c.uconf = uconf
	c.cn = uconf[:nickname][instance]

	c.twit = decompose_twit(status.text, botname)
	c.text = c.twit[:body]
	c.except_me = c.twit[:except_me]
	c.to_me = c.twit[:to_me]
	c.only_to_me = c.twit[:only_to_me]

	# パラメータ計算
	# 好感度: 基本0〜100 ランダム-10〜+10
	# ツンデレ: 基本-25〜+25 ランダム-25〜+25
	c.mop = moon_phase(@curr_obj)
	c.affection = uconf[:affection] + (rand(20) - 10)
	c.tsundere = uconf[:tsundere] +
	    (25 * Math::cos(c.mop * Math::PI/15)).to_i

	return c
    end

    #----------------------------------------------------------------------
    SECONDS_PER_DAY = 86400
    MIDNIGHT_AT_JST = 54000	# = (-9 * 3600) % SECONDS_PER_DAY
    NARUHODO_AT_JST = 68400	# = (-13 * 3600) % SECONDS_PER_DAY
    def routine_process_locked()
	super

	# JSTで午前0時の2〜60秒前だった場合、よるほ待機する
	t = Time.now.to_i
	if t % SECONDS_PER_DAY < MIDNIGHT_AT_JST - 2 &&
		t % SECONDS_PER_DAY >= MIDNIGHT_AT_JST - 60
	    sleep(MIDNIGHT_AT_JST - t % SECONDS_PER_DAY - 1)
	    post_status("よるほー")
	elsif t % SECONDS_PER_DAY < NARUHODO_AT_JST - 2 &&
		t % SECONDS_PER_DAY >= NARUHODO_AT_JST - 60
	    sleep(NARUHODO_AT_JST - t % SECONDS_PER_DAY - 1)
	    post_status("なるほど四時じゃねーの♪ …あ、あれっ？ #4ji")
	end
    end

    def post_ban_state_changed(curr_ban_state)
	case curr_ban_state
	when true # 規制始まった
	    @logger.info("post ban started")
	    @post_ban_start = @curr_time

	when false # 規制終わった
	    @logger.info("post ban ended")
	    minutes = (@curr_time - @post_ban_start) / 60
	    if minutes >= 15
		resp = Time.at(@post_ban_start).strftime("%m月%d日%H時%M分")
		resp += "から#{minutes}分間ポスト規制されてたみたい…"
		resp += rand_item(["もう、みんな激しすぎだよぉ…" + tere_aa(),
				   "しゃべれなくてごめんね",
				   #"いつか規制を壊しちゃおっと…",
				   "っ http://scarlets.dyndns.info/flan_bot.html"])
		args = {:status => resp}
		invoke_api("update_status", "text=#{resp}") do
		    @client.update_status(args)
		end
	    end
	end
    end

    #----------------------------------------------------------------------
    # その他ユーティリティ

    def moon_phase(day)
	# 出典: http://ja.wikipedia.org/wiki/%E6%9C%88%E9%BD%A2
	# もともと近似式であるが、さらに月齢と月相を同一視する
	# より正確には http://www1.odn.ne.jp/kentaurus/mooncal.htm
	# 0:朔, 2:三日月, 14:満月, 15:十六夜
	return (((day.year - 11) % 19) * 11 + MOON_PHASE_OFFSETS[day.month - 1] + day.day) % 30
    end

    MOON_PHASE_OFFSETS = [0, 2, 0, 2, 2, 4, 5, 6, 7, 8, 9, 10]

    # 乱数発生
    # dice(n, m)=m面ダイスをn回振る dice(m)=m面ダイスを1回振る
    def dice(a, b = 0)
	if b == 0
	    m = a; count = 1
	else
	    m = b; count = a
	end
	sum = 0
	count.times { sum += rand(m) }
	return sum + count
    end

    # テレ顔のAAをランダムに返す
    def tere_aa()
	return rand_item(['(///ω///)', '(//∇//)', '（*/∇＼*）', '(*ﾉωﾉ)',
			  '＞△＜', '(*／ω＼*)'])
    end

    public

    # コマンドラインを分析
    def process_command_line()
	super
    end
end

#----------------------------------------------------------------------
# コンストラクタでARGVを受け取れるようにしたFranBotクラス

class CliFranBot < FranBot
    def initialize(argv)
	new_argv = argv.clone
	botname = new_argv.shift
	super(new_argv, "data_#{botname}", botname)
    end
end

#----------------------------------------------------------------------
# 複数のフランbotをまとめた仮想的なbot

class FranBotCluster
    include BotLauncher

    ROUTINE_INTERVAL = TwitterBot::ROUTINE_INTERVAL

    def initialize(argv)
	# TODO オプションでインスタンスを指定できるようにする
	@instances = []
	FranBot::FRANBOT_LIST.each_with_index do |name, instance|
	    new_argv = argv.clone
	    @instances << FranBot.new(new_argv, "data_#{name}", name)
	end
	@argv = argv.clone
    end

    def login(arg)
	@instances.each {|x| x.login(arg)}
    end

    def routine_process()
	@instances.each {|x| x.routine_process()}
    end

    def continue_loop?()
	@instances.each {|x| return false if x.continue_loop?() == false}
	return true
    end

    def wait_for_next_run()
	# TODO: ログ
	sleep(ROUTINE_INTERVAL)
    end

    def refresh_intervals()
	@instances.each {|x| x.refresh_intervals(arg)}
    end

    def process_command_line
	super
    end
end

bot = CliFranBot.new(ARGV)
#bot = FranBotCluster.new(ARGV)
bot.process_command_line()

# TODO:
# かなこん
# 「帰宅した「ら」」などの仮定に反応しないようにする
# フォーオブアカインドで爆発
# フォーオブアカインドでMAGIごっこ
# 規制かかったら他のbotがお知らせする
# 規制解除されたら出迎える
# uconf[:last_user_talk_at]使ってない
