#!/usr/bin/env ruby -Ku
require 'rubygems'
require 'oauth'
require 'yaml/store'

# http://twitter.com/oauth_clients
# に行ってアプリケーションを登録し、このスクリプトを実行する

acct = YAML::Store.new('config_v2_new.yaml')
acct.transaction do
    # コンシューマ
    puts "Enter consumery key: "
    acct[:consumer_key] = gets.chomp
    puts "Enter consumery secret: "
    acct[:consumer_secret] = gets.chomp
    consumer = OAuth::Consumer.new(acct[:consumer_key],
				   acct[:consumer_secret],
				   {:site => 'http://twitter.com'})

    # リクエストトークン
    request_token = consumer.get_request_token

    # アクセストークン
    url = request_token.authorize_url

    puts "Visit following URL: "
    puts url

    puts "Enter verifier number (from above site): "
    verifier = gets.chomp

    access_token = request_token.get_access_token(:oauth_verifier => verifier)
    acct[:access_token] = access_token.token
    acct[:access_token_secret] = access_token.secret

    # その他
    acct[:user_name] = 'FIX_ME'
    acct[:password] = 'FIX_ME'
    acct[:use_oauth] = true
    acct[:debug] = true

    acct.commit()
end
