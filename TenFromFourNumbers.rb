require 'rational'

# 数値と演算子を用いて別の指定した数値を作る式を探索する
class TFFN
    attr_accessor :n_list, :target, :results
    attr_accessor :depth_limit, :results_limit, :time_limit
    attr_accessor :trace

    # 初期設定。
    # 探索深度を20、探索を打ち切る結果数を5とする
    def initialize(n_list, target)
	@n_list = n_list
	@target = target
	@depth_limit = 20
	@results_limit = 5
	@time_limit = nil
	@trace = false
    end

    # 再帰的に条件に合う式を探索する
    def try_recursive(expr)
	# 発見できたかのチェック
	if @n_used.index(false) == nil &&
		expr.value.size == 1 &&
		expr.value[0] == @target
	    print "found: #{expr.to_s}\n" if @trace
	    @results << expr
	end

	# 一定の条件を満たしたら探索を打ち切り
	# (1) 一定以上の探索の深さ (2) 解の個数 (3) 時間
	return if expr.notation.size >= @depth_limit
	return if @results.size >= @results_limit
	return if @time_limit != nil && @time_limit <= Time.now.to_i

	# 数値を追加する試行
	@n_list.size.times do |index|
	    next if @n_used[index] == true
	    @n_used[index] = true
	    try_numeric(expr, @n_list[index])
	    @n_used[index] = false
	end

	# 演算子を追加する試行
	if expr.has_2_operands?
	    try_2oper(expr, :add) {|x,y|
		# x y neg add は x y subに統一
		break nil if expr.notation[-1] == :neg
		Expr.normalize(x + y)
	    }
	    try_2oper(expr, :sub) {|x,y|
		# x y neg sub は x y addに統一
		break nil if expr.notation[-1] == :neg
		Expr.normalize(x - y)
	    }
	    try_2oper(expr, :mul) {|x,y|
		# x y neg mulはx neg y mulに統一
		break nil if expr.notation[-1] == :neg
		Expr.normalize(x * y)
	    }
	    try_2oper(expr, :div) {|x,y|
		break nil if expr.notation[-1] == :neg
		break nil if y == 0
		y = Rational(y, 1) unless y.is_a?(Rational)
		Expr.normalize(x / y)
	    }
	    try_2oper(expr, :pow) {|x,y|
		# 指数は整数の場合に限定する
		# (指数が非整数でx<0の場合はEDOMとなるが、これは自動的に除外)
		break nil if !y.is_a?(Integer)

		# 基数が0の場合、指数が負ならゼロ除算、0なら未定義、でなければ0
		if x == 0
		    break (y <= 0) ? nil : 0
		end

		# 大きすぎる値を除外
		if x.is_a?(Integer)
		    break nil if (y * Math.log(x.abs)).abs > 100
		else
		    break nil if (y * Math.log(x.numerator.abs)).abs > 7
		    break nil if (y * Math.log(x.denominator.abs)).abs > 7
		end
		Expr.normalize(x ** y)
	    }
	end
	if expr.has_1_operand?
	    try_1oper(expr, :sqrt) {|x|
		# sqrt(0)=0, sqrt(1)=1なので整数に関しては冪等->適用させない
		# sqrt(2)とsqrt(3)は有理数にならないので使わない
		# 有理数に関しては、√(y/x)=√y/√xにまとめ、
		# 根が整数になる範囲で適用する
		break nil if x < 4 || x > 1000
		Expr.sqrt_i(x)
	    }
	    try_1oper(expr, :fact) {|x|
		# fact(1)=1,fact(2)=2なので冪等->適用させない
		#break nil if expr.notation[-1] == :fact
		break nil if !x.is_a?(Integer)
		break nil if x < 0 || x == 1 || x == 2 || x > 15
		Expr.factorial(x)
	    }
	    try_1oper(expr, :neg) {|x|
		# neg negは元に戻る->適用させない
		break nil if expr.notation[-1] == :neg
		# x y sub neg -> y x sub
		break nil if expr.notation[-1] == :sub
		break nil if x == 0
		-x
	    } if @allow_negation
	end
    end

    def try_numeric(expr, n)
	print "#{expr.inspect}, trying #{n}\n" if @trace
	expr2 = expr.dup()
	expr2.notation << n
	expr2.value << n
	try_recursive(expr2)
    end

    def try_2oper(expr, sym)
	expr2 = expr.dup()
	y = expr2.value.pop(); x = expr2.value.pop()
	z = yield(x, y)
	if z != nil
	    print "#{expr.inspect}, trying #{x} #{sym} #{y}\n" if @trace
	    expr2.notation << sym
	    expr2.value << z
	    try_recursive(expr2)
	end
    end

    def try_1oper(expr, sym)
	expr2 = expr.dup()
	x = expr2.value.pop()
	z = yield(x)
	if z != nil
	    print "#{expr.inspect}, trying #{x} #{sym}\n" if @trace
	    expr2.notation << sym
	    expr2.value << z
	    try_recursive(expr2)
	end
    end

    def search()
	@results = []
	@n_used = @n_list.map {|x| false}

	@allow_negation = false
	try_recursive(Expr.empty_expr())
	@allow_negation = true
	try_recursive(Expr.empty_expr())
    end

    # 以下の項からなる式のクラス。
    # - n0 n1 n2 n3の4つの数字
    # - 単項演算子: -, !, √
    # - 二項演算子: +, -, *, /, ^
    #
    # 式は逆ポーランド形式で表現し、かつ必ずしも完結していない。
    #
    # 内部的には、表現と評価結果を保持する。
    # notation=[1, 2, 3, +], value=[1,5]
    # notation=[1, 2, 3, +, -], value=[-4]
    class Expr
	attr_accessor :notation, :value

	def initialize(notation, value)
	    @notation = notation
	    @value = value
	end

	def dup()
	    return Expr.new(self.notation.dup(), self.value.dup())
	end

	def to_s()
	    return "[" + notation.join(",") + "]"
	end

	def inspect()
	    return "note=[" + notation.join(",") + "]" + 
		" value=[" + value.join(',') + "]"
	end

	# 空のExprインスタンスを作成して返す
	def self.empty_expr()
	    return Expr.new([], [])
	end

	# 評価スタック上に2つのオペランドがあるか調べる
	def has_2_operands?()
	    return @value.size >= 2 &&
		@value[-2].is_a?(Numeric) && @value[-1].is_a?(Numeric)
	end

	# 評価スタック上にオペランドがあるか調べる
	def has_1_operand?()
	    return @value.size >= 1 && @value[-1].is_a?(Numeric)
	end

	# RationalインスタンスがIntegerに変換できる場合は変換し、
	# できない場合は元のインスタンス自体を返す
	# (Rationalインスタンスは自動的に約分されるが、
	# 自動的にはIntegerに変換されない)
	def self.normalize(x)
	    if x.is_a?(Rational)
		return +x.numerator if x.denominator == 1
		return -x.numerator if x.denominator == -1
	    end
	    return x
	end

	# 非負整数の階乗を計算する
	def self.factorial(n)
	    return 1 if n == 0
	    return n * factorial(n - 1)
	end

	# 答えが整数になる範囲で平方根を求める。
	def self.sqrt_i(n)
	    r = Math.sqrt(n).to_i
	    return r if r * r == n
	    return nil
	end

	# 以下の結合強度に従い、式を整形する
	#  0 項そのもの, カッコ
	#  1 !    (階乗)
	#  2 √   (平方根)
	#  3 * /  (乗除)
	#  4 **   (累乗)
	#  5 -    (単項マイナス)
	#  6 + -  (加減)
	def pp()
	    tmp = []
	    @notation.each do |node|
		case node
		when Numeric
		    tmp.push([0, "#{node}"])
		when :add
		    format_2(tmp, 6) {|lhs,rhs| "#{lhs}+#{rhs}"}
		when :sub
		    format_2(tmp, 6) {|lhs,rhs| "#{lhs}-#{rhs}"}
		when :mul
		    format_2(tmp, 3) {|lhs,rhs| "#{lhs}*#{rhs}"}
		when :div
		    format_2(tmp, 3) {|lhs,rhs| "#{lhs}/#{rhs}"}
		when :pow
		    format_2(tmp, 4) {|lhs,rhs| "#{lhs}**#{rhs}"}
		when :sqrt
		    format_1(tmp, 2) {|term| "√#{term}"}
		when :fact
		    format_1(tmp, 1) {|term| "#{term}!"}
		when :neg
		    format_1(tmp, 5) {|term| "-#{term}"}
		end
 	    end
	    return tmp[0][1]
	end

	def format_2(array, pri)
	    rhs, lhs = array.pop(), array.pop()
	    lhs[1] = "(#{lhs[1]})" if lhs[0] >= pri
	    rhs[1] = "(#{rhs[1]})" if rhs[0] >= pri
	    result = yield(lhs[1], rhs[1])
	    array.push([pri, result])
	end

	def format_1(array, pri)
	    term = array.pop()
	    term[1] = "(#{term[1]})" if term[0] >= pri
	    result = yield(term[1])
	    array.push([pri, result])
	end
    end
end

if false
    [[0,5,0,0], [0,4,9,5], [3,9,8,3], [1,2,1,2],[1,1,4,1]].each do |list|
	tffn = TFFN.new(list, 10)
	#tffn.trace = true
	tffn.results_limit = 3
	tffn.search()
	tffn.results().each do |x|
	    print "#{list.inspect()}: #{x} -> #{x.pp()}\n"
	end
    end
end
