#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

require 'TenFromFourNumbers'

class FranBot < TwitterBot
    INHIBITED_BOMB_TARGET = /(#{FRAN_RE}|あんじっぷ|あんぬっぷ|あんじぷ|fs495|ccs_sf_net)/ox

    # 爆発
    def bakuhatsu(c)
	# xxx(は|なんか)(、) 爆発(しろ|しなさい|しちゃえ)
	# xxx(を)	(、) 爆発させて
	#
	return false if c.except_me

	# 助詞を含む場合と含まない場合があるので、変換時には助詞を追加しない
	if !(c.text =~ /爆発させてる/x) &&
		c.text =~ /(.*?)、?爆発(しろ|しなさい|しちゃえ|させて)/x
	    # target=読点を除いた爆発対象物。
	    target = $1

	    # 直近の24時間に6回までしか爆発させられない
	    c.uconf[:bomb_history].reject! {|t| t < @curr_time - 86400}
	    if c.nm != @admin && c.uconf[:bomb_history].size > 5
		@logger.warn("@#{c.nm} tried bombing too often")
		return false 
	    end
	    c.uconf[:bomb_history] << @curr_time

	    if target =~ INHIBITED_BOMB_TARGET
		# 爆発防止
		target = "ひっどーい！#{c.cn}のほうこそ"
		c.uconf[:affection] = 0
		c.uconf[:tsundere] = -25

	    elsif target =~ /(.*)#{FIRST_PERSON}(.*)/ox
		# ユーザ自身を爆発させる場合は、人称を変換して形容詞も追加する
		# xxxな/オレ/のyyy/爆発しろ -> xxxな/ドM/name/のyyy
		adj = rand_item(['ドM', 'ド変態', 'いじられキャラ',
				 '受けキャラ', '総受け', 'ヘタレ受け'])
		target = "#{$1}#{adj}#{c.cn}#{$3}"
	    end

	    spell = rand_item(SPELL_CARDS)
	    pre = rand_item(['いくよ', 'くらえ', '覚悟してね'])
	    resp = rand_item(["壊しちゃお！#{pre}！#{spell}",
			      "壊れちゃえ！#{pre}！#{spell}",
			      "爆発しちゃえ！#{pre}！#{spell}",
			      "、きゅっとしてどっかーん！",
			      "、#{spell}をプレゼントしてあげる！",
			      "、#{spell}でイッちゃえ！"])
	    post_status("@#{c.nm} #{target}#{resp}", c.status)
	    c.uconf[:affection] -= dice(1, 10) # 爆発させると好感度下がる
	    return true
	end
	return false
    end

    # ネタ系
    def neta(c)
	rt = "RT @#{c.nm}: #{c.rawtext}"

	case c.text
	    # フランちゃんうふふ
	when /(うふふ|ウフフ|ｳﾌﾌ)/x
	    # 直近の24時間に6回までしかうふふさせられない
	    c.uconf[:ufufu_history].reject! {|t| t < @curr_time - 86499}
	    if c.nm != @admin && c.uconf[:bomb_history].size > 5
		return false
	    end
	    c.uconf[:ufufu_history] << @curr_time

	    post_status("@#{c.nm} うふふ♥", c.status)
	    c.uconf[:affection] += dice(2, 4)
	    c.uconf[:tsundere] += dice(1, 4)
	    return true

	    # 診断メーカー。次の条件にひっかかってしまうので、先に判定
	when %r(このつぶやきが.*以内にRTされなかったらフランドール・スカーレットは.*の嫁になります。 http://shindanmaker.com/24471)
	    if c.uconf[:affection] < 90
		post_status("阻止♪ #{rt}", c.status)
	    end
	    return true

	when %r(以内に.*RTされたら.*(フラン|レミリア|パチ|アリス|こいし|諏訪子|ぬえ).*を描きます！ http://.* #yomeijimeR18)
	    post_status("ｗｋｔｋ #{rt}", c.status)
	    return true

	    # 診断メーカー 基本好感度が高いユーザに対してのみ1/64の確率で反応
	when %r(http://shindanmaker.com/(\d+))
	    id = $1.to_i
	    if !@gconf[:shindan_maker].include?(id) &&
		    c.uconf[:affection] > 80 &&
		    rand(64) == 0
		resp = get_shindan_maker(id, botname)
		if resp != nil
		    post_status(resp) 
		    @gconf[:shindan_maker] << id # ポスト成功したら更新
		end
		return true
	    end

	end

	#------------------------------------------------------------
	# 以下は、他ユーザ向けでなければ反応する
	return false if c.except_me

	# ぬるぽガッ
	n = c.text.scan(/(ぬるぽ|ヌルポ|ﾇﾙﾎﾟ|nullpo|Nullpo)/x).length
	if n > 0
	    post_status("ｶﾞｯ" * n + "♪ #{rt}", c.status)
	    return true
	end

	case c.text
	    # マジックテープ
	when /(バリバリ|ﾊﾞﾘﾊﾞﾘ)/x
	    if c.text =~ /(仕事|勉強|原稿|頑張る|がんばる|するぞ|やるぞ)/x
		post_status("やめ…がんばって！ #{rt}", c.status)
	    else
		post_status("やめて！ #{rt}", c.status)
	    end
	    return true

	    # ねこフラン
	when /(にゃ(あ|ー)|ニャ(ア|ー)|\(「=・ｴ・\)「)/x
	    # 直近24時間に6回までしかにゃーできない
	    c.uconf[:mew_history].reject! {|t| t < @curr_time - 86400}
	    return true if c.uconf[:mew_history].size > 5
	    c.uconf[:mew_history] << @curr_time

	    post_status("@#{c.nm} にゃー♪ http://www.nicovideo.jp/watch/sm6817006", c.status)
	    c.uconf[:affection] += dice(2, 4)
	    c.uconf[:tsundere] += dice(1, 4)
	    return true

	    # メロンパンうめぇｗ
	when /^(.*)うめぇｗ$/x
	    post_status("@#{c.nm} #{$1}もぐもぐｗｗｗｗ#{$1}うまいｗｗｗｗ#{$1}#{$1}うますぎワロタｗｗｗｗ#{$1}もぐもぐｗｗｗｗ#{$1}もぐもぐｗｗｗｗ#{$1}ｗｗｗｗ", c.status)
	    return true

	when /(えび|エビ|海老)(フラン|ふらん)(ﾜﾛﾀ|ワロタ|わろた)/x
	    post_status("@#{c.nm} うぅ、よく言われるけどどういう意味なの？", c.status)
	    return true

	    # onatter.jp
	when /オナニーなう\ \#onatter/x
	    post_status("@#{c.nm} |дﾟ)ｼﾞｰｯ #{rt}", c.status)
	    return true
	when /ふぅ・・・\ \#onatter/x
	    post_status("@#{c.nm} |彡ｻｯ #{rt}", c.status)
	    return true

	    # レミリアとのからみトリガ
	when /.*#{FRAN_RE}.*眠(い|くなった)(\?|\？)/ox
	    post_status("少し眠いかな？ ふぁぁぁ…", c.status)
	    return true

	    # ＼RT連鎖／ 反応確率は1/4
	when /＼(.*?)／/x
	    if !c.rawtext.include?("@#{WS}*#{botname}") && rand(4) == 0
		post_status("＼#{$1}／ #{rt}", c.status)
		return true
	    end
	end

	#------------------------------------------------------------
	# 以下はbotあての時のみ反応
	return false if not c.to_me

	case c.text
	when /(.*)(肉片|肉塊|挽肉|挽き肉|ひき肉|ミンチ|微塵|みじん)にして/
	    target = $1
	    if target =~ INHIBITED_BOMB_TARGET
		resp = "ひっどーい！#{c.cn}のほうこそぎゅっとしてどっかーん☆"
		c.uconf[:affection] = 0
		c.uconf[:tsundere] = -25
	    else
		resp = rand_item(["分かった！えいっ♪",
				  "うん！どっか〜ん☆"])
	    end
	    post_status("#{resp} #{rt}", c.status)
	    return true

	when /ドロワース/x
	    post_status("@#{c.nm}ドロワー「ズ」は複数形だよっ！", c.status)
	    return true

	when /ドロワーズ.*(mogmog|もぐもぐ|モグモグ|ﾓｸﾞﾓｸﾞ)/x
	    post_status("変態＞＜ #{rt}", c.status)
	    return true

	when /(\d+)\s+(\d+)\s+(\d+)\s+(\d+)で(\d+)/x
	    tffn = TFFN.new([$1.to_i, $2.to_i, $3.to_i, $4.to_i], $5.to_i)
	    tffn.results_limit = 1
	    tffn.time_limit = Time.now.to_i + 10
	    tffn.search()
	    results = tffn.results()
	    if results.size == 0
		post_status("うーん、すぐにはわかんないなあ…ごめんね #{rt}", c.status)
	    else
		s = results[0].pp()
		resp = rand_item(["#{s}とかどうかな？",
				  "わかった！#{s}かな？",
				  "うーん、#{s}とかどう？"])
		post_status("#{resp} #{rt}", c.status)
	    end
	    return true
	end

	return false
    end
end
