include private/secret.mk

bot_list = FrandleBot FlandreBot frandle_bot flandre_bot

#----------------------------------------------------------------------

install:
	cp -p [A-Z]*.rb $(targetdir)
	cp -p run-franbot.sh $(targetdir)

# Web用に公開する
publish: publish-script publish-my-web

publish-script:
	cp -p [A-Z]*.rb $(publishdir)/1st-gen
	cp -p *.sh Makefile $(publishdir)/1st-gen

publish-my-web:
	cp -p doc/FrandleBot.html $(publishdir)

# 本番環境の稼働データをテスト環境にインポートする
import: import-bot-data-all import-user-data

import-bot-data-all:
	for bot in $(bot_list); do \
		$(MAKE) import-bot-data BOT=$$bot; \
	done

import-bot-data:
	rsync -a $(targetdir)/data_$(BOT)/*.yaml data_$(BOT)
	rsync -a $(targetdir)/data_$(BOT)/twitterbot.log data_$(BOT)
	-mv      $(targetdir)/data_$(BOT)/twitterbot.log.* data_$(BOT)

import-user-data:
	rsync -ar $(targetdir)/data_shared .

count:
	$(MAKE) import > /dev/null 2>&1 && ruby ./count-ff.rb

# テスト環境の稼働データを本番環境にエクスポートする
export: export-bot-data-all export-user-data

export-bot-data-all:
	for bot in $(bot_list); do \
		$(MAKE) export-bot-data BOT=$$bot; \
	done

export-bot-data:
	rsync -a data_$(BOT)/*.yaml $(targetdir)/data_$(BOT)
	rsync -a data_$(BOT)/twitterbot.log $(targetdir)/data_$(BOT)

export-user-data:
	rsync -ar data_shared $(targetdir)

move-log:
	-for bot in $(bot_list); do \
		chmod 444 data_$$bot/twitterbot.log.*; \
		mv -i data_$$bot/twitterbot.log.* data_$$bot/old-logs/; \
	done

compress-log:
	-for bot in $(bot_list); do \
		bzip2 --best data_$$bot/old-logs/twitterbot.log.*; \
	done

fixperm:
	chown -R $(hguser) $(hgrepo)
	chmod -R g+w $(hgrepo)

clean:
	rm -f *~*~

zip:
	zip -r `date +%Y%m%d-%H%M%S`.zip *.rb *.sh Makefile doc
	zip -r `date +%Y%m%d-%H%M%S`log.zip *and*ot/twitterbot.log*
	mv 2010*.zip $(ssldir)

out-all:
	$(MAKE) install export zip publish
