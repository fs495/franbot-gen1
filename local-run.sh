#!/bin/sh

set -e
PID_FILE=private/local_pid

start_bot() {
    echo "Starting FranBot..."
    echo "########################################" >> exception.log
    date "+Started at %Y-%m-%d %H:%M:%S" >> exception.log

    cp /dev/null $PID_FILE
    ruby -Ku FranBot.rb FrandleBot -daemon &
    echo $! >> $PID_FILE
    ruby -Ku FranBot.rb FlandreBot -daemon &
    echo $! >> $PID_FILE
    ruby -Ku FranBot.rb frandle_bot -daemon &
    echo $! >> $PID_FILE
    ruby -Ku FranBot.rb flandre_bot -daemon &
    echo $! >> $PID_FILE
}

stop_bot() {
    echo "Stopping FranBot..."
    date "+Stopped at %Y-%m-%d %H:%M:%S" >> exception.log
    set +e
    while read pid; do
	echo " Terminating process... $pid"
	kill $pid
    done < $PID_FILE
    set -e
}

case $1 in
    start)
	start_bot
	;;
    stop)
	stop_bot
	;;
    restart)
	stop_bot
	start_bot
	;;
    stats|ps)
	while read pid; do
	    ps -f -p $pid
	done < $PID_FILE
	;;
esac
