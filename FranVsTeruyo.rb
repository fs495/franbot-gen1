#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    #----------------------------------------------------------------------
    # 輝夜bot専用の応答処理
    def respond_for_teruyo_bot(status, uconf)
	rt = "RT @#{TERUYO_BOT}: #{status.text}"
	tere = tere_aa()

	#------------------------------------------------------------
	# 無条件で応答する処理をまず実行

	case status.text
	when /フラン先生のサイトに拍手コメしなきゃ/
	    resp = rand_item(["またこの@#{TERUYO_BOT}って人がコメ付けてる…暇人なんだなあ",
			      "わーい、また褒めてもらっちゃった♥",
			      "かわいい絵ですねって言われるのも飽きちゃったなあ… #{tere}"])
	    post_status("#{resp} #{rt}", status)
	    return

	when /レミ×フラの続きが気になるわ・・・/
	    resp = rand_item(["私の妄想を絵にするのが追いつかない #{tere}",
			      "今度はフラ×レミも描いてみたいなあ…",
			      "続きはお姉様陵辱ものにしようかなあ"])
	    post_status("#{resp} #{rt}", status)
	    return

	when /今度イベントがあるみたいね・・・フラン先生がサークル参加/
	    resp = rand_item(["うーん、お姉様がお外に出してくれるかなあ…",
			      "次のイベント…落としそう orz..."])
	    post_status("#{resp} #{rt}", status)
	    return
	end

	#------------------------------------------------------------
	# ポスト規制値に近づいていたら処理終了
	if too_many_posts?(0.3)
	    @logger.warn("suppressed response for TeruyoBOT")
	    return
	end

    end

end
