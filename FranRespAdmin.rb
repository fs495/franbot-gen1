#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class FranBot < TwitterBot
    # フォロー解除要求を処理する
    def unfollow_request(c)
	if c.only_to_me &&
		c.text =~ /^((unfollow|remove|フォロー解除|リムーブ)して|別れよう|絶交だ|さよう?なら)$/x
	    remove_friend(c.nm)
	    post_status("@#{c.nm} くすん…;;", c.status)

	    # 次回以降のmention処理をこのステータス以降から開始するようにする
	    if @gconf[:mention_last_id] < c.status.id
		@gconf[:mention_last_id] = c.status.id
	    end

	    return true
	end
	return false
    end

    # 管理コマンド
    def administration(c)
	# 強制終了
	if c.to_me && c.text =~ /^バルス(!|！)?$/x
	    if c.nm == @admin
		post_status("@#{c.nm} ちょっと疲れちゃった…しばらくお休みするね")
		@quit_flag = true
	    else
		spell = rand_item(SPELL_CARDS)
		post_status("@#{c.nm} 滅びの言葉は#{c.cn}に必要みたいだね！#{spell}", c.status)
	    end
	    return true
	end

	# ヘックスダンプ
	if c.rawtext =~ /@(#{FRANBOT_LIST.join("|")})(.*)hex$/ox
	    # 不可視スペースがあるかもしれないのでBHは使わずにマッチさせる
	    resp = $2.unpack('U*').map{|n| n.to_s(16)}.join(',')
	    post_status("@#{c.nm} #{resp}")
	    return true
	end

	# ここ以降は明示的にbotあての場合のみ
	return false unless c.to_me

	case c.text
	    # bot主から伝言
	when /^Msg:#{WS}*(.*)/ox
	    if c.nm == @admin
		post_status("お世話係から伝言だよ♪「#{$1}」")
		return true
	    end

	    # botとして発言
	when /^Fwd:#{WS}*(.*)/ox
	    if c.nm == @admin
		post_status("#{$1}")
		return true
	    end

	when /^Block:#{WS}*(.*)/ox
	    if c.nm == @admin
		invoke_api("block", $1) do
		    @client.block($1)
		end
		return true
	    end

	when /^Power-Off#{WS}*([0-9:]+)#{WS}*([0-9]+)/
	    if c.nm == @admin
		start, duration = $1, $2
		post_status("@#{c.nm} power off at #{start}, power on after #{duration} seconds", c.status)
		system("echo /usr/bin/sudo /usr/sbin/rtcwake -l -m no -s #{duration} | at #{start}")
		system("echo /usr/bin/sudo /sbin/poweroff | at #{start}")
		return true
	    end

	when /^Power-Off/
	    if c.nm == @admin
		post_status("あ、ごめんね。ちょっとお休みするね", c.status)
		system('/usr/bin/sudo', '/sbin/poweroff')
		@quit_flag = true
		return true
	    end

	    # botステータス報告
	when /^ステータス(教えて|おしえて|リセット)$/x
	    resp = "@#{c.nm}"
	    if @rate_limit_info != nil
		l = @rate_limit_info
		resp += " get系:curr #{l.remaining_hits}"
		resp += ",min #{@gconf[:fetch_min_remain]}"
		resp += ",capacity #{l.hourly_limit}"
	    end
	    resp += " post系:curr #{MAX_UPDATES - @gconf[:update_history].size}"
	    resp += ",min #{@gconf[:post_min_remain]}"
	    resp += ",capacity #{MAX_UPDATES}"

	    if $1 == 'リセット' && c.nm == @admin
		@gconf[:fetch_min_remain] = 9999
		@gconf[:post_min_remain] = 9999
		resp += " リセットしたよ"
	    end
	    post_status(resp, c.status)
	    return true

	    # ユーザステータス設定
	when /ステータス変更\s*([-+]?\d+)\s*[,，、\s]\s*([-+]?\d+)/x
	    c.uconf[:affection] = $1.to_i
	    c.uconf[:tsundere] = $2.to_i
	    post_status("@#{c.nm} 基本好感度を#{c.uconf[:affection]}、基本ツンデレ度を#{c.uconf[:tsundere]}に変更したよー", c.status)
	    return true

	    # ユーザステータス報告
	when /#{FIRST_PERSON}のこと.*どう(思|おも)(う|ってる)(\?|\？)/ox
	    if c.affection > 90
		if c.tsundere > +10
		    resp = "#{c.cn}のこと大好きだよ" + tere_aa()
		elsif c.tsundere < -10
		    resp = "………べっ、別に#{c.cn}のことなんかどうとも思ってないんだからねっ" + tere_aa()
		else
		    resp = "#{c.cn}は大事なお友達だよ♪"
		end
	    elsif c.affection > 60
		if c.tsundere < -10
		    resp = "#{c.cn}は食料として優秀だよね♪"
		else
		    resp = "#{c.cn}は大事なお友達だよ♪"
		end
	    elsif c.affection > 30
		resp = "え、#{c.cn}はお友達だよね？"
	    else
		resp = "#{c.cn}とはなかよしになったばかりだよね"
	    end
	    resp += " [好感度#{c.affection}(基本#{c.uconf[:affection]}),"
	    resp += " ツンデレ度#{c.tsundere}(基本#{c.uconf[:tsundere]})]"
	    post_status("@#{c.nm} #{resp}", c.status)
	    return true

	    # 遅延状況を調べる
	when /遅延(調べて|しらべて|教えて|おしえて)/x
	    if c.nm == @admin || c.affection > 90
		s = Time.now.strftime("%Y-%m-%d %H:%M:%S")
		post_status("禁弾「過去を刻む時計」(#{s})")
		return true
	    end

	    # かなこん
	when /^kanakon (.*)$/
	    return false if c.nm != @admin

	    key = $1.to_sym
	    if key == :reset
		list = []
		KANAKON.each_pair {|k,v| list |= v}
		unregister_kanakon(list)
		return true
	    elsif KANAKON[key] != nil
		register_kanakon(KANAKON[key])
		return true
	    end
	end

	return false
    end
end
